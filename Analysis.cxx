#define Analysis_cxx
#include "Analysis.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>

void Analysis::Loop()
{
//   In a ROOT session, you can do:
//      root> .L Analysis.C
//      root> Analysis t
//      root> t.GetEntry(12); // Fill t data members with entry number 12
//      root> t.Show();       // Show values of entry 12
//      root> t.Show(16);     // Read and show values of entry 16
//      root> t.Loop();       // Loop on all entries
//

//     This is the loop skeleton where:
//    jentry is the global entry number in the chain
//    ientry is the entry number in the current Tree
//  Note that the argument to GetEntry must be:
//    jentry for TChain::GetEntry
//    ientry for TTree::GetEntry and TBranch::GetEntry
//
//       To read only selected branches, Insert statements like:
// METHOD1:
//    fChain->SetBranchStatus("*",0);  // disable all branches
//    fChain->SetBranchStatus("branchname",1);  // activate branchname
// METHOD2: replace line
//    fChain->GetEntry(jentry);       //read all branches
//by  b_branchname->GetEntry(ientry); //read only this branch
   if (fChain == 0) return;

   TH1F* h_mass = new TH1F("mass","mass",10,0,200);
   
   Long64_t nentries = fChain->GetEntriesFast();

   Long64_t nbytes = 0, nb = 0;
   for (Long64_t jentry=0; jentry<1;jentry++) {
      Long64_t ientry = LoadTree(jentry);
      if (ientry < 0) break;
      nb = fChain->GetEntry(jentry);   nbytes += nb;
      // if (Cut(ientry) < 0) continue;

      //fChain->SetBranchStatus("*",0); //disable all branches
      //fChain->SetBranchStatus("Particle_",1);
      int size = sizeof(Particle_PID) / sizeof(int);

      //std::cout << "Event weight: " << *Event_Weight << " " << "Weight_Weight: " << *Weight_Weight << std::endl;
      if(*Weight_Weight == 0)
	std::cout << "Event weight: " << *Event_Weight << " " << "Weight_Weight: " << *Weight_Weight << std::endl; 

      int part_W_ind = -1;
      int part_Z_ind = -1;
      
      for(int ii=0; ii<size; ii++){

	if((Particle_Status[ii]>=21 && Particle_Status[ii]<=22))
	  std::cout << "particle " << ii << " pid: " << Particle_PID[ii] << " particle M1: " << Particle_M1[ii] << " particle M1 PID: " << Particle_PID[Particle_M1[ii]] << " particle status: " << Particle_Status[ii] << std::endl;
	if(Particle_Status[ii]==62 && (Particle_PID[ii]==23 || fabs(Particle_PID[ii])==24)){

	  if(Particle_PID[ii]==23)
	    part_Z_ind = ii;
	  if(fabs(Particle_PID[ii])==24)
            part_W_ind = ii;
	  
	  std::cout << "particle " << ii << " pid: " << Particle_PID[ii] << " particle M1: " << Particle_M1[ii] << " particle M1 PID: " << Particle_PID[Particle_M1[ii]] << " particle status: " << Particle_Status[ii] << std::endl;
	}

      }
      
      
      std::cout << "particle " << Particle_D1[part_Z_ind] << " pid: " << Particle_PID[Particle_D1[part_Z_ind]] << " particle M1: " << Particle_M1[Particle_D1[part_Z_ind]] << " particle M1 PID: " << Particle_PID[Particle_M1[Particle_D1[part_Z_ind]]] << " particle status: " << Particle_Status[Particle_D1[part_Z_ind]] << std::endl;

      std::cout << "particle " << Particle_D1[part_Z_ind] << " pid: " << Particle_PID[Particle_D2[part_Z_ind]] << " particle M1: " << Particle_M1[Particle_D2[part_Z_ind]] << " particle M1 PID: " << Particle_PID[Particle_M1[Particle_D2[part_Z_ind]]] << " particle status: " << Particle_Status[Particle_D2[part_Z_ind]] << std::endl;

      std::cout << "particle " << Particle_D1[part_W_ind] << " pid: " << Particle_PID[Particle_D1[part_W_ind]] << " particle M1: " << Particle_M1[Particle_D1[part_W_ind]] << " particle M1 PID: " << Particle_PID[Particle_M1[Particle_D1[part_W_ind]]] << " particle status: " << Particle_Status[Particle_D1[part_W_ind]] << std::endl;
      
      std::cout << "particle " << Particle_D1[part_W_ind] << " pid: " << Particle_PID[Particle_D2[part_W_ind]] << " particle M1: " << Particle_M1[Particle_D2[part_W_ind]] << " particle M1 PID: " << Particle_PID[Particle_M1[Particle_D2[part_W_ind]]] << " particle status: " << Particle_Status[Particle_D2[part_W_ind]] << std::endl;
      
   }

   TFile *file = new TFile("output.root","RECREATE");
   file->cd();
   h_mass->Write();
   file->Close();
}
