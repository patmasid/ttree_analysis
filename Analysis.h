//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Jun  5 10:46:25 2024 by ROOT version 6.30/06
// from TTree Delphes/Analysis tree
// found on file: ntuple_delphes_wqqzll_ATLAS_13TeV_ptll200.root
//////////////////////////////////////////////////////////

#ifndef Analysis_h
#define Analysis_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "TClonesArray.h"
#include "TObject.h"

class Analysis {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.
   static constexpr Int_t kMaxEvent = 1;
   static constexpr Int_t kMaxWeight = 2;
   static constexpr Int_t kMaxParticle = 3536;
   static constexpr Int_t kMaxTrack = 179;
   static constexpr Int_t kMaxTower = 477;
   static constexpr Int_t kMaxEFlowTrack = 179;
   static constexpr Int_t kMaxEFlowPhoton = 272;
   static constexpr Int_t kMaxEFlowNeutralHadron = 176;
   static constexpr Int_t kMaxGenJet04 = 11;
   static constexpr Int_t kMaxGenJet10 = 11;
   static constexpr Int_t kMaxCaloJet04 = 11;
   static constexpr Int_t kMaxCaloJet10 = 10;
   static constexpr Int_t kMaxParticleFlowJet04 = 12;
   static constexpr Int_t kMaxParticleFlowJet10 = 11;
   static constexpr Int_t kMaxGenMissingET = 1;
   static constexpr Int_t kMaxJet = 11;
   static constexpr Int_t kMaxElectron = 4;
   static constexpr Int_t kMaxPhoton = 2;
   static constexpr Int_t kMaxMuon = 3;
   static constexpr Int_t kMaxMissingET = 1;
   static constexpr Int_t kMaxScalarHT = 1;

   // Declaration of leaf types
   Int_t           Event_;
   UInt_t          Event_fUniqueID[kMaxEvent];   //[Event_]
   UInt_t          Event_fBits[kMaxEvent];   //[Event_]
   Long64_t        Event_Number[kMaxEvent];   //[Event_]
   Float_t         Event_ReadTime[kMaxEvent];   //[Event_]
   Float_t         Event_ProcTime[kMaxEvent];   //[Event_]
   Int_t           Event_ProcessID[kMaxEvent];   //[Event_]
   Int_t           Event_MPI[kMaxEvent];   //[Event_]
   Float_t         Event_Weight[kMaxEvent];   //[Event_]
   Float_t         Event_CrossSection[kMaxEvent];   //[Event_]
   Float_t         Event_CrossSectionError[kMaxEvent];   //[Event_]
   Float_t         Event_Scale[kMaxEvent];   //[Event_]
   Float_t         Event_AlphaQED[kMaxEvent];   //[Event_]
   Float_t         Event_AlphaQCD[kMaxEvent];   //[Event_]
   Int_t           Event_ID1[kMaxEvent];   //[Event_]
   Int_t           Event_ID2[kMaxEvent];   //[Event_]
   Float_t         Event_X1[kMaxEvent];   //[Event_]
   Float_t         Event_X2[kMaxEvent];   //[Event_]
   Float_t         Event_ScalePDF[kMaxEvent];   //[Event_]
   Float_t         Event_PDF1[kMaxEvent];   //[Event_]
   Float_t         Event_PDF2[kMaxEvent];   //[Event_]
   Int_t           Event_size;
   Int_t           Weight_;
   UInt_t          Weight_fUniqueID[kMaxWeight];   //[Weight_]
   UInt_t          Weight_fBits[kMaxWeight];   //[Weight_]
   Float_t         Weight_Weight[kMaxWeight];   //[Weight_]
   Int_t           Weight_size;
   Int_t           Particle_;
   UInt_t          Particle_fUniqueID[kMaxParticle];   //[Particle_]
   UInt_t          Particle_fBits[kMaxParticle];   //[Particle_]
   Int_t           Particle_PID[kMaxParticle];   //[Particle_]
   Int_t           Particle_Status[kMaxParticle];   //[Particle_]
   Int_t           Particle_IsPU[kMaxParticle];   //[Particle_]
   Int_t           Particle_M1[kMaxParticle];   //[Particle_]
   Int_t           Particle_M2[kMaxParticle];   //[Particle_]
   Int_t           Particle_D1[kMaxParticle];   //[Particle_]
   Int_t           Particle_D2[kMaxParticle];   //[Particle_]
   Int_t           Particle_Charge[kMaxParticle];   //[Particle_]
   Float_t         Particle_Mass[kMaxParticle];   //[Particle_]
   Float_t         Particle_E[kMaxParticle];   //[Particle_]
   Float_t         Particle_Px[kMaxParticle];   //[Particle_]
   Float_t         Particle_Py[kMaxParticle];   //[Particle_]
   Float_t         Particle_Pz[kMaxParticle];   //[Particle_]
   Float_t         Particle_P[kMaxParticle];   //[Particle_]
   Float_t         Particle_PT[kMaxParticle];   //[Particle_]
   Float_t         Particle_Eta[kMaxParticle];   //[Particle_]
   Float_t         Particle_Phi[kMaxParticle];   //[Particle_]
   Float_t         Particle_Rapidity[kMaxParticle];   //[Particle_]
   Float_t         Particle_T[kMaxParticle];   //[Particle_]
   Float_t         Particle_X[kMaxParticle];   //[Particle_]
   Float_t         Particle_Y[kMaxParticle];   //[Particle_]
   Float_t         Particle_Z[kMaxParticle];   //[Particle_]
   Int_t           Particle_size;
   Int_t           Track_;
   UInt_t          Track_fUniqueID[kMaxTrack];   //[Track_]
   UInt_t          Track_fBits[kMaxTrack];   //[Track_]
   Int_t           Track_PID[kMaxTrack];   //[Track_]
   Int_t           Track_Charge[kMaxTrack];   //[Track_]
   Float_t         Track_P[kMaxTrack];   //[Track_]
   Float_t         Track_PT[kMaxTrack];   //[Track_]
   Float_t         Track_Eta[kMaxTrack];   //[Track_]
   Float_t         Track_Phi[kMaxTrack];   //[Track_]
   Float_t         Track_CtgTheta[kMaxTrack];   //[Track_]
   Float_t         Track_C[kMaxTrack];   //[Track_]
   Float_t         Track_Mass[kMaxTrack];   //[Track_]
   Float_t         Track_EtaOuter[kMaxTrack];   //[Track_]
   Float_t         Track_PhiOuter[kMaxTrack];   //[Track_]
   Float_t         Track_T[kMaxTrack];   //[Track_]
   Float_t         Track_X[kMaxTrack];   //[Track_]
   Float_t         Track_Y[kMaxTrack];   //[Track_]
   Float_t         Track_Z[kMaxTrack];   //[Track_]
   Float_t         Track_TOuter[kMaxTrack];   //[Track_]
   Float_t         Track_XOuter[kMaxTrack];   //[Track_]
   Float_t         Track_YOuter[kMaxTrack];   //[Track_]
   Float_t         Track_ZOuter[kMaxTrack];   //[Track_]
   Float_t         Track_Xd[kMaxTrack];   //[Track_]
   Float_t         Track_Yd[kMaxTrack];   //[Track_]
   Float_t         Track_Zd[kMaxTrack];   //[Track_]
   Float_t         Track_XFirstHit[kMaxTrack];   //[Track_]
   Float_t         Track_YFirstHit[kMaxTrack];   //[Track_]
   Float_t         Track_ZFirstHit[kMaxTrack];   //[Track_]
   Float_t         Track_L[kMaxTrack];   //[Track_]
   Float_t         Track_D0[kMaxTrack];   //[Track_]
   Float_t         Track_DZ[kMaxTrack];   //[Track_]
   Float_t         Track_Nclusters[kMaxTrack];   //[Track_]
   Float_t         Track_dNdx[kMaxTrack];   //[Track_]
   Float_t         Track_ErrorP[kMaxTrack];   //[Track_]
   Float_t         Track_ErrorPT[kMaxTrack];   //[Track_]
   Float_t         Track_ErrorPhi[kMaxTrack];   //[Track_]
   Float_t         Track_ErrorCtgTheta[kMaxTrack];   //[Track_]
   Float_t         Track_ErrorT[kMaxTrack];   //[Track_]
   Float_t         Track_ErrorD0[kMaxTrack];   //[Track_]
   Float_t         Track_ErrorDZ[kMaxTrack];   //[Track_]
   Float_t         Track_ErrorC[kMaxTrack];   //[Track_]
   Float_t         Track_ErrorD0Phi[kMaxTrack];   //[Track_]
   Float_t         Track_ErrorD0C[kMaxTrack];   //[Track_]
   Float_t         Track_ErrorD0DZ[kMaxTrack];   //[Track_]
   Float_t         Track_ErrorD0CtgTheta[kMaxTrack];   //[Track_]
   Float_t         Track_ErrorPhiC[kMaxTrack];   //[Track_]
   Float_t         Track_ErrorPhiDZ[kMaxTrack];   //[Track_]
   Float_t         Track_ErrorPhiCtgTheta[kMaxTrack];   //[Track_]
   Float_t         Track_ErrorCDZ[kMaxTrack];   //[Track_]
   Float_t         Track_ErrorCCtgTheta[kMaxTrack];   //[Track_]
   Float_t         Track_ErrorDZCtgTheta[kMaxTrack];   //[Track_]
   TRef            Track_Particle[kMaxTrack];
   Int_t           Track_VertexIndex[kMaxTrack];   //[Track_]
   Int_t           Track_size;
   Int_t           Tower_;
   UInt_t          Tower_fUniqueID[kMaxTower];   //[Tower_]
   UInt_t          Tower_fBits[kMaxTower];   //[Tower_]
   Float_t         Tower_ET[kMaxTower];   //[Tower_]
   Float_t         Tower_Eta[kMaxTower];   //[Tower_]
   Float_t         Tower_Phi[kMaxTower];   //[Tower_]
   Float_t         Tower_E[kMaxTower];   //[Tower_]
   Float_t         Tower_T[kMaxTower];   //[Tower_]
   Float_t         Tower_X[kMaxTower];   //[Tower_]
   Float_t         Tower_Y[kMaxTower];   //[Tower_]
   Float_t         Tower_Z[kMaxTower];   //[Tower_]
   Int_t           Tower_NTimeHits[kMaxTower];   //[Tower_]
   Float_t         Tower_Eem[kMaxTower];   //[Tower_]
   Float_t         Tower_Ehad[kMaxTower];   //[Tower_]
   Float_t         Tower_Etrk[kMaxTower];   //[Tower_]
   Float_t         Tower_Edges[kMaxTower][4];   //[Tower_]
   TRefArray       Tower_Particles[kMaxTower];
   Int_t           Tower_size;
   Int_t           EFlowTrack_;
   UInt_t          EFlowTrack_fUniqueID[kMaxEFlowTrack];   //[EFlowTrack_]
   UInt_t          EFlowTrack_fBits[kMaxEFlowTrack];   //[EFlowTrack_]
   Int_t           EFlowTrack_PID[kMaxEFlowTrack];   //[EFlowTrack_]
   Int_t           EFlowTrack_Charge[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_P[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_PT[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_Eta[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_Phi[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_CtgTheta[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_C[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_Mass[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_EtaOuter[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_PhiOuter[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_T[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_X[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_Y[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_Z[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_TOuter[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_XOuter[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_YOuter[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_ZOuter[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_Xd[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_Yd[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_Zd[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_XFirstHit[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_YFirstHit[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_ZFirstHit[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_L[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_D0[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_DZ[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_Nclusters[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_dNdx[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_ErrorP[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_ErrorPT[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_ErrorPhi[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_ErrorCtgTheta[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_ErrorT[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_ErrorD0[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_ErrorDZ[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_ErrorC[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_ErrorD0Phi[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_ErrorD0C[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_ErrorD0DZ[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_ErrorD0CtgTheta[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_ErrorPhiC[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_ErrorPhiDZ[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_ErrorPhiCtgTheta[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_ErrorCDZ[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_ErrorCCtgTheta[kMaxEFlowTrack];   //[EFlowTrack_]
   Float_t         EFlowTrack_ErrorDZCtgTheta[kMaxEFlowTrack];   //[EFlowTrack_]
   TRef            EFlowTrack_Particle[kMaxEFlowTrack];
   Int_t           EFlowTrack_VertexIndex[kMaxEFlowTrack];   //[EFlowTrack_]
   Int_t           EFlowTrack_size;
   Int_t           EFlowPhoton_;
   UInt_t          EFlowPhoton_fUniqueID[kMaxEFlowPhoton];   //[EFlowPhoton_]
   UInt_t          EFlowPhoton_fBits[kMaxEFlowPhoton];   //[EFlowPhoton_]
   Float_t         EFlowPhoton_ET[kMaxEFlowPhoton];   //[EFlowPhoton_]
   Float_t         EFlowPhoton_Eta[kMaxEFlowPhoton];   //[EFlowPhoton_]
   Float_t         EFlowPhoton_Phi[kMaxEFlowPhoton];   //[EFlowPhoton_]
   Float_t         EFlowPhoton_E[kMaxEFlowPhoton];   //[EFlowPhoton_]
   Float_t         EFlowPhoton_T[kMaxEFlowPhoton];   //[EFlowPhoton_]
   Float_t         EFlowPhoton_X[kMaxEFlowPhoton];   //[EFlowPhoton_]
   Float_t         EFlowPhoton_Y[kMaxEFlowPhoton];   //[EFlowPhoton_]
   Float_t         EFlowPhoton_Z[kMaxEFlowPhoton];   //[EFlowPhoton_]
   Int_t           EFlowPhoton_NTimeHits[kMaxEFlowPhoton];   //[EFlowPhoton_]
   Float_t         EFlowPhoton_Eem[kMaxEFlowPhoton];   //[EFlowPhoton_]
   Float_t         EFlowPhoton_Ehad[kMaxEFlowPhoton];   //[EFlowPhoton_]
   Float_t         EFlowPhoton_Etrk[kMaxEFlowPhoton];   //[EFlowPhoton_]
   Float_t         EFlowPhoton_Edges[kMaxEFlowPhoton][4];   //[EFlowPhoton_]
   TRefArray       EFlowPhoton_Particles[kMaxEFlowPhoton];
   Int_t           EFlowPhoton_size;
   Int_t           EFlowNeutralHadron_;
   UInt_t          EFlowNeutralHadron_fUniqueID[kMaxEFlowNeutralHadron];   //[EFlowNeutralHadron_]
   UInt_t          EFlowNeutralHadron_fBits[kMaxEFlowNeutralHadron];   //[EFlowNeutralHadron_]
   Float_t         EFlowNeutralHadron_ET[kMaxEFlowNeutralHadron];   //[EFlowNeutralHadron_]
   Float_t         EFlowNeutralHadron_Eta[kMaxEFlowNeutralHadron];   //[EFlowNeutralHadron_]
   Float_t         EFlowNeutralHadron_Phi[kMaxEFlowNeutralHadron];   //[EFlowNeutralHadron_]
   Float_t         EFlowNeutralHadron_E[kMaxEFlowNeutralHadron];   //[EFlowNeutralHadron_]
   Float_t         EFlowNeutralHadron_T[kMaxEFlowNeutralHadron];   //[EFlowNeutralHadron_]
   Float_t         EFlowNeutralHadron_X[kMaxEFlowNeutralHadron];   //[EFlowNeutralHadron_]
   Float_t         EFlowNeutralHadron_Y[kMaxEFlowNeutralHadron];   //[EFlowNeutralHadron_]
   Float_t         EFlowNeutralHadron_Z[kMaxEFlowNeutralHadron];   //[EFlowNeutralHadron_]
   Int_t           EFlowNeutralHadron_NTimeHits[kMaxEFlowNeutralHadron];   //[EFlowNeutralHadron_]
   Float_t         EFlowNeutralHadron_Eem[kMaxEFlowNeutralHadron];   //[EFlowNeutralHadron_]
   Float_t         EFlowNeutralHadron_Ehad[kMaxEFlowNeutralHadron];   //[EFlowNeutralHadron_]
   Float_t         EFlowNeutralHadron_Etrk[kMaxEFlowNeutralHadron];   //[EFlowNeutralHadron_]
   Float_t         EFlowNeutralHadron_Edges[kMaxEFlowNeutralHadron][4];   //[EFlowNeutralHadron_]
   TRefArray       EFlowNeutralHadron_Particles[kMaxEFlowNeutralHadron];
   Int_t           EFlowNeutralHadron_size;
   Int_t           GenJet04_;
   UInt_t          GenJet04_fUniqueID[kMaxGenJet04];   //[GenJet04_]
   UInt_t          GenJet04_fBits[kMaxGenJet04];   //[GenJet04_]
   Float_t         GenJet04_PT[kMaxGenJet04];   //[GenJet04_]
   Float_t         GenJet04_Eta[kMaxGenJet04];   //[GenJet04_]
   Float_t         GenJet04_Phi[kMaxGenJet04];   //[GenJet04_]
   Float_t         GenJet04_T[kMaxGenJet04];   //[GenJet04_]
   Float_t         GenJet04_Mass[kMaxGenJet04];   //[GenJet04_]
   Float_t         GenJet04_DeltaEta[kMaxGenJet04];   //[GenJet04_]
   Float_t         GenJet04_DeltaPhi[kMaxGenJet04];   //[GenJet04_]
   UInt_t          GenJet04_Flavor[kMaxGenJet04];   //[GenJet04_]
   UInt_t          GenJet04_FlavorAlgo[kMaxGenJet04];   //[GenJet04_]
   UInt_t          GenJet04_FlavorPhys[kMaxGenJet04];   //[GenJet04_]
   UInt_t          GenJet04_TauFlavor[kMaxGenJet04];   //[GenJet04_]
   UInt_t          GenJet04_BTag[kMaxGenJet04];   //[GenJet04_]
   UInt_t          GenJet04_BTagAlgo[kMaxGenJet04];   //[GenJet04_]
   UInt_t          GenJet04_BTagPhys[kMaxGenJet04];   //[GenJet04_]
   UInt_t          GenJet04_TauTag[kMaxGenJet04];   //[GenJet04_]
   Float_t         GenJet04_TauWeight[kMaxGenJet04];   //[GenJet04_]
   Int_t           GenJet04_Charge[kMaxGenJet04];   //[GenJet04_]
   Float_t         GenJet04_EhadOverEem[kMaxGenJet04];   //[GenJet04_]
   Int_t           GenJet04_NCharged[kMaxGenJet04];   //[GenJet04_]
   Int_t           GenJet04_NNeutrals[kMaxGenJet04];   //[GenJet04_]
   Float_t         GenJet04_NeutralEnergyFraction[kMaxGenJet04];   //[GenJet04_]
   Float_t         GenJet04_ChargedEnergyFraction[kMaxGenJet04];   //[GenJet04_]
   Float_t         GenJet04_Beta[kMaxGenJet04];   //[GenJet04_]
   Float_t         GenJet04_BetaStar[kMaxGenJet04];   //[GenJet04_]
   Float_t         GenJet04_MeanSqDeltaR[kMaxGenJet04];   //[GenJet04_]
   Float_t         GenJet04_PTD[kMaxGenJet04];   //[GenJet04_]
   Float_t         GenJet04_FracPt[kMaxGenJet04][5];   //[GenJet04_]
   Float_t         GenJet04_Tau[kMaxGenJet04][5];   //[GenJet04_]
   TLorentzVector  GenJet04_SoftDroppedJet[kMaxGenJet04];
   TLorentzVector  GenJet04_SoftDroppedSubJet1[kMaxGenJet04];
   TLorentzVector  GenJet04_SoftDroppedSubJet2[kMaxGenJet04];
   TLorentzVector  GenJet04_TrimmedP4[5][kMaxGenJet04];
   TLorentzVector  GenJet04_PrunedP4[5][kMaxGenJet04];
   TLorentzVector  GenJet04_SoftDroppedP4[5][kMaxGenJet04];
   Int_t           GenJet04_NSubJetsTrimmed[kMaxGenJet04];   //[GenJet04_]
   Int_t           GenJet04_NSubJetsPruned[kMaxGenJet04];   //[GenJet04_]
   Int_t           GenJet04_NSubJetsSoftDropped[kMaxGenJet04];   //[GenJet04_]
   Double_t        GenJet04_ExclYmerge12[kMaxGenJet04];   //[GenJet04_]
   Double_t        GenJet04_ExclYmerge23[kMaxGenJet04];   //[GenJet04_]
   Double_t        GenJet04_ExclYmerge34[kMaxGenJet04];   //[GenJet04_]
   Double_t        GenJet04_ExclYmerge45[kMaxGenJet04];   //[GenJet04_]
   Double_t        GenJet04_ExclYmerge56[kMaxGenJet04];   //[GenJet04_]
   TRefArray       GenJet04_Constituents[kMaxGenJet04];
   TRefArray       GenJet04_Particles[kMaxGenJet04];
   TLorentzVector  GenJet04_Area[kMaxGenJet04];
   Int_t           GenJet04_size;
   Int_t           GenJet10_;
   UInt_t          GenJet10_fUniqueID[kMaxGenJet10];   //[GenJet10_]
   UInt_t          GenJet10_fBits[kMaxGenJet10];   //[GenJet10_]
   Float_t         GenJet10_PT[kMaxGenJet10];   //[GenJet10_]
   Float_t         GenJet10_Eta[kMaxGenJet10];   //[GenJet10_]
   Float_t         GenJet10_Phi[kMaxGenJet10];   //[GenJet10_]
   Float_t         GenJet10_T[kMaxGenJet10];   //[GenJet10_]
   Float_t         GenJet10_Mass[kMaxGenJet10];   //[GenJet10_]
   Float_t         GenJet10_DeltaEta[kMaxGenJet10];   //[GenJet10_]
   Float_t         GenJet10_DeltaPhi[kMaxGenJet10];   //[GenJet10_]
   UInt_t          GenJet10_Flavor[kMaxGenJet10];   //[GenJet10_]
   UInt_t          GenJet10_FlavorAlgo[kMaxGenJet10];   //[GenJet10_]
   UInt_t          GenJet10_FlavorPhys[kMaxGenJet10];   //[GenJet10_]
   UInt_t          GenJet10_TauFlavor[kMaxGenJet10];   //[GenJet10_]
   UInt_t          GenJet10_BTag[kMaxGenJet10];   //[GenJet10_]
   UInt_t          GenJet10_BTagAlgo[kMaxGenJet10];   //[GenJet10_]
   UInt_t          GenJet10_BTagPhys[kMaxGenJet10];   //[GenJet10_]
   UInt_t          GenJet10_TauTag[kMaxGenJet10];   //[GenJet10_]
   Float_t         GenJet10_TauWeight[kMaxGenJet10];   //[GenJet10_]
   Int_t           GenJet10_Charge[kMaxGenJet10];   //[GenJet10_]
   Float_t         GenJet10_EhadOverEem[kMaxGenJet10];   //[GenJet10_]
   Int_t           GenJet10_NCharged[kMaxGenJet10];   //[GenJet10_]
   Int_t           GenJet10_NNeutrals[kMaxGenJet10];   //[GenJet10_]
   Float_t         GenJet10_NeutralEnergyFraction[kMaxGenJet10];   //[GenJet10_]
   Float_t         GenJet10_ChargedEnergyFraction[kMaxGenJet10];   //[GenJet10_]
   Float_t         GenJet10_Beta[kMaxGenJet10];   //[GenJet10_]
   Float_t         GenJet10_BetaStar[kMaxGenJet10];   //[GenJet10_]
   Float_t         GenJet10_MeanSqDeltaR[kMaxGenJet10];   //[GenJet10_]
   Float_t         GenJet10_PTD[kMaxGenJet10];   //[GenJet10_]
   Float_t         GenJet10_FracPt[kMaxGenJet10][5];   //[GenJet10_]
   Float_t         GenJet10_Tau[kMaxGenJet10][5];   //[GenJet10_]
   TLorentzVector  GenJet10_SoftDroppedJet[kMaxGenJet10];
   TLorentzVector  GenJet10_SoftDroppedSubJet1[kMaxGenJet10];
   TLorentzVector  GenJet10_SoftDroppedSubJet2[kMaxGenJet10];
   TLorentzVector  GenJet10_TrimmedP4[5][kMaxGenJet10];
   TLorentzVector  GenJet10_PrunedP4[5][kMaxGenJet10];
   TLorentzVector  GenJet10_SoftDroppedP4[5][kMaxGenJet10];
   Int_t           GenJet10_NSubJetsTrimmed[kMaxGenJet10];   //[GenJet10_]
   Int_t           GenJet10_NSubJetsPruned[kMaxGenJet10];   //[GenJet10_]
   Int_t           GenJet10_NSubJetsSoftDropped[kMaxGenJet10];   //[GenJet10_]
   Double_t        GenJet10_ExclYmerge12[kMaxGenJet10];   //[GenJet10_]
   Double_t        GenJet10_ExclYmerge23[kMaxGenJet10];   //[GenJet10_]
   Double_t        GenJet10_ExclYmerge34[kMaxGenJet10];   //[GenJet10_]
   Double_t        GenJet10_ExclYmerge45[kMaxGenJet10];   //[GenJet10_]
   Double_t        GenJet10_ExclYmerge56[kMaxGenJet10];   //[GenJet10_]
   TRefArray       GenJet10_Constituents[kMaxGenJet10];
   TRefArray       GenJet10_Particles[kMaxGenJet10];
   TLorentzVector  GenJet10_Area[kMaxGenJet10];
   Int_t           GenJet10_size;
   Int_t           CaloJet04_;
   UInt_t          CaloJet04_fUniqueID[kMaxCaloJet04];   //[CaloJet04_]
   UInt_t          CaloJet04_fBits[kMaxCaloJet04];   //[CaloJet04_]
   Float_t         CaloJet04_PT[kMaxCaloJet04];   //[CaloJet04_]
   Float_t         CaloJet04_Eta[kMaxCaloJet04];   //[CaloJet04_]
   Float_t         CaloJet04_Phi[kMaxCaloJet04];   //[CaloJet04_]
   Float_t         CaloJet04_T[kMaxCaloJet04];   //[CaloJet04_]
   Float_t         CaloJet04_Mass[kMaxCaloJet04];   //[CaloJet04_]
   Float_t         CaloJet04_DeltaEta[kMaxCaloJet04];   //[CaloJet04_]
   Float_t         CaloJet04_DeltaPhi[kMaxCaloJet04];   //[CaloJet04_]
   UInt_t          CaloJet04_Flavor[kMaxCaloJet04];   //[CaloJet04_]
   UInt_t          CaloJet04_FlavorAlgo[kMaxCaloJet04];   //[CaloJet04_]
   UInt_t          CaloJet04_FlavorPhys[kMaxCaloJet04];   //[CaloJet04_]
   UInt_t          CaloJet04_TauFlavor[kMaxCaloJet04];   //[CaloJet04_]
   UInt_t          CaloJet04_BTag[kMaxCaloJet04];   //[CaloJet04_]
   UInt_t          CaloJet04_BTagAlgo[kMaxCaloJet04];   //[CaloJet04_]
   UInt_t          CaloJet04_BTagPhys[kMaxCaloJet04];   //[CaloJet04_]
   UInt_t          CaloJet04_TauTag[kMaxCaloJet04];   //[CaloJet04_]
   Float_t         CaloJet04_TauWeight[kMaxCaloJet04];   //[CaloJet04_]
   Int_t           CaloJet04_Charge[kMaxCaloJet04];   //[CaloJet04_]
   Float_t         CaloJet04_EhadOverEem[kMaxCaloJet04];   //[CaloJet04_]
   Int_t           CaloJet04_NCharged[kMaxCaloJet04];   //[CaloJet04_]
   Int_t           CaloJet04_NNeutrals[kMaxCaloJet04];   //[CaloJet04_]
   Float_t         CaloJet04_NeutralEnergyFraction[kMaxCaloJet04];   //[CaloJet04_]
   Float_t         CaloJet04_ChargedEnergyFraction[kMaxCaloJet04];   //[CaloJet04_]
   Float_t         CaloJet04_Beta[kMaxCaloJet04];   //[CaloJet04_]
   Float_t         CaloJet04_BetaStar[kMaxCaloJet04];   //[CaloJet04_]
   Float_t         CaloJet04_MeanSqDeltaR[kMaxCaloJet04];   //[CaloJet04_]
   Float_t         CaloJet04_PTD[kMaxCaloJet04];   //[CaloJet04_]
   Float_t         CaloJet04_FracPt[kMaxCaloJet04][5];   //[CaloJet04_]
   Float_t         CaloJet04_Tau[kMaxCaloJet04][5];   //[CaloJet04_]
   TLorentzVector  CaloJet04_SoftDroppedJet[kMaxCaloJet04];
   TLorentzVector  CaloJet04_SoftDroppedSubJet1[kMaxCaloJet04];
   TLorentzVector  CaloJet04_SoftDroppedSubJet2[kMaxCaloJet04];
   TLorentzVector  CaloJet04_TrimmedP4[5][kMaxCaloJet04];
   TLorentzVector  CaloJet04_PrunedP4[5][kMaxCaloJet04];
   TLorentzVector  CaloJet04_SoftDroppedP4[5][kMaxCaloJet04];
   Int_t           CaloJet04_NSubJetsTrimmed[kMaxCaloJet04];   //[CaloJet04_]
   Int_t           CaloJet04_NSubJetsPruned[kMaxCaloJet04];   //[CaloJet04_]
   Int_t           CaloJet04_NSubJetsSoftDropped[kMaxCaloJet04];   //[CaloJet04_]
   Double_t        CaloJet04_ExclYmerge12[kMaxCaloJet04];   //[CaloJet04_]
   Double_t        CaloJet04_ExclYmerge23[kMaxCaloJet04];   //[CaloJet04_]
   Double_t        CaloJet04_ExclYmerge34[kMaxCaloJet04];   //[CaloJet04_]
   Double_t        CaloJet04_ExclYmerge45[kMaxCaloJet04];   //[CaloJet04_]
   Double_t        CaloJet04_ExclYmerge56[kMaxCaloJet04];   //[CaloJet04_]
   TRefArray       CaloJet04_Constituents[kMaxCaloJet04];
   TRefArray       CaloJet04_Particles[kMaxCaloJet04];
   TLorentzVector  CaloJet04_Area[kMaxCaloJet04];
   Int_t           CaloJet04_size;
   Int_t           CaloJet10_;
   UInt_t          CaloJet10_fUniqueID[kMaxCaloJet10];   //[CaloJet10_]
   UInt_t          CaloJet10_fBits[kMaxCaloJet10];   //[CaloJet10_]
   Float_t         CaloJet10_PT[kMaxCaloJet10];   //[CaloJet10_]
   Float_t         CaloJet10_Eta[kMaxCaloJet10];   //[CaloJet10_]
   Float_t         CaloJet10_Phi[kMaxCaloJet10];   //[CaloJet10_]
   Float_t         CaloJet10_T[kMaxCaloJet10];   //[CaloJet10_]
   Float_t         CaloJet10_Mass[kMaxCaloJet10];   //[CaloJet10_]
   Float_t         CaloJet10_DeltaEta[kMaxCaloJet10];   //[CaloJet10_]
   Float_t         CaloJet10_DeltaPhi[kMaxCaloJet10];   //[CaloJet10_]
   UInt_t          CaloJet10_Flavor[kMaxCaloJet10];   //[CaloJet10_]
   UInt_t          CaloJet10_FlavorAlgo[kMaxCaloJet10];   //[CaloJet10_]
   UInt_t          CaloJet10_FlavorPhys[kMaxCaloJet10];   //[CaloJet10_]
   UInt_t          CaloJet10_TauFlavor[kMaxCaloJet10];   //[CaloJet10_]
   UInt_t          CaloJet10_BTag[kMaxCaloJet10];   //[CaloJet10_]
   UInt_t          CaloJet10_BTagAlgo[kMaxCaloJet10];   //[CaloJet10_]
   UInt_t          CaloJet10_BTagPhys[kMaxCaloJet10];   //[CaloJet10_]
   UInt_t          CaloJet10_TauTag[kMaxCaloJet10];   //[CaloJet10_]
   Float_t         CaloJet10_TauWeight[kMaxCaloJet10];   //[CaloJet10_]
   Int_t           CaloJet10_Charge[kMaxCaloJet10];   //[CaloJet10_]
   Float_t         CaloJet10_EhadOverEem[kMaxCaloJet10];   //[CaloJet10_]
   Int_t           CaloJet10_NCharged[kMaxCaloJet10];   //[CaloJet10_]
   Int_t           CaloJet10_NNeutrals[kMaxCaloJet10];   //[CaloJet10_]
   Float_t         CaloJet10_NeutralEnergyFraction[kMaxCaloJet10];   //[CaloJet10_]
   Float_t         CaloJet10_ChargedEnergyFraction[kMaxCaloJet10];   //[CaloJet10_]
   Float_t         CaloJet10_Beta[kMaxCaloJet10];   //[CaloJet10_]
   Float_t         CaloJet10_BetaStar[kMaxCaloJet10];   //[CaloJet10_]
   Float_t         CaloJet10_MeanSqDeltaR[kMaxCaloJet10];   //[CaloJet10_]
   Float_t         CaloJet10_PTD[kMaxCaloJet10];   //[CaloJet10_]
   Float_t         CaloJet10_FracPt[kMaxCaloJet10][5];   //[CaloJet10_]
   Float_t         CaloJet10_Tau[kMaxCaloJet10][5];   //[CaloJet10_]
   TLorentzVector  CaloJet10_SoftDroppedJet[kMaxCaloJet10];
   TLorentzVector  CaloJet10_SoftDroppedSubJet1[kMaxCaloJet10];
   TLorentzVector  CaloJet10_SoftDroppedSubJet2[kMaxCaloJet10];
   TLorentzVector  CaloJet10_TrimmedP4[5][kMaxCaloJet10];
   TLorentzVector  CaloJet10_PrunedP4[5][kMaxCaloJet10];
   TLorentzVector  CaloJet10_SoftDroppedP4[5][kMaxCaloJet10];
   Int_t           CaloJet10_NSubJetsTrimmed[kMaxCaloJet10];   //[CaloJet10_]
   Int_t           CaloJet10_NSubJetsPruned[kMaxCaloJet10];   //[CaloJet10_]
   Int_t           CaloJet10_NSubJetsSoftDropped[kMaxCaloJet10];   //[CaloJet10_]
   Double_t        CaloJet10_ExclYmerge12[kMaxCaloJet10];   //[CaloJet10_]
   Double_t        CaloJet10_ExclYmerge23[kMaxCaloJet10];   //[CaloJet10_]
   Double_t        CaloJet10_ExclYmerge34[kMaxCaloJet10];   //[CaloJet10_]
   Double_t        CaloJet10_ExclYmerge45[kMaxCaloJet10];   //[CaloJet10_]
   Double_t        CaloJet10_ExclYmerge56[kMaxCaloJet10];   //[CaloJet10_]
   TRefArray       CaloJet10_Constituents[kMaxCaloJet10];
   TRefArray       CaloJet10_Particles[kMaxCaloJet10];
   TLorentzVector  CaloJet10_Area[kMaxCaloJet10];
   Int_t           CaloJet10_size;
   Int_t           ParticleFlowJet04_;
   UInt_t          ParticleFlowJet04_fUniqueID[kMaxParticleFlowJet04];   //[ParticleFlowJet04_]
   UInt_t          ParticleFlowJet04_fBits[kMaxParticleFlowJet04];   //[ParticleFlowJet04_]
   Float_t         ParticleFlowJet04_PT[kMaxParticleFlowJet04];   //[ParticleFlowJet04_]
   Float_t         ParticleFlowJet04_Eta[kMaxParticleFlowJet04];   //[ParticleFlowJet04_]
   Float_t         ParticleFlowJet04_Phi[kMaxParticleFlowJet04];   //[ParticleFlowJet04_]
   Float_t         ParticleFlowJet04_T[kMaxParticleFlowJet04];   //[ParticleFlowJet04_]
   Float_t         ParticleFlowJet04_Mass[kMaxParticleFlowJet04];   //[ParticleFlowJet04_]
   Float_t         ParticleFlowJet04_DeltaEta[kMaxParticleFlowJet04];   //[ParticleFlowJet04_]
   Float_t         ParticleFlowJet04_DeltaPhi[kMaxParticleFlowJet04];   //[ParticleFlowJet04_]
   UInt_t          ParticleFlowJet04_Flavor[kMaxParticleFlowJet04];   //[ParticleFlowJet04_]
   UInt_t          ParticleFlowJet04_FlavorAlgo[kMaxParticleFlowJet04];   //[ParticleFlowJet04_]
   UInt_t          ParticleFlowJet04_FlavorPhys[kMaxParticleFlowJet04];   //[ParticleFlowJet04_]
   UInt_t          ParticleFlowJet04_TauFlavor[kMaxParticleFlowJet04];   //[ParticleFlowJet04_]
   UInt_t          ParticleFlowJet04_BTag[kMaxParticleFlowJet04];   //[ParticleFlowJet04_]
   UInt_t          ParticleFlowJet04_BTagAlgo[kMaxParticleFlowJet04];   //[ParticleFlowJet04_]
   UInt_t          ParticleFlowJet04_BTagPhys[kMaxParticleFlowJet04];   //[ParticleFlowJet04_]
   UInt_t          ParticleFlowJet04_TauTag[kMaxParticleFlowJet04];   //[ParticleFlowJet04_]
   Float_t         ParticleFlowJet04_TauWeight[kMaxParticleFlowJet04];   //[ParticleFlowJet04_]
   Int_t           ParticleFlowJet04_Charge[kMaxParticleFlowJet04];   //[ParticleFlowJet04_]
   Float_t         ParticleFlowJet04_EhadOverEem[kMaxParticleFlowJet04];   //[ParticleFlowJet04_]
   Int_t           ParticleFlowJet04_NCharged[kMaxParticleFlowJet04];   //[ParticleFlowJet04_]
   Int_t           ParticleFlowJet04_NNeutrals[kMaxParticleFlowJet04];   //[ParticleFlowJet04_]
   Float_t         ParticleFlowJet04_NeutralEnergyFraction[kMaxParticleFlowJet04];   //[ParticleFlowJet04_]
   Float_t         ParticleFlowJet04_ChargedEnergyFraction[kMaxParticleFlowJet04];   //[ParticleFlowJet04_]
   Float_t         ParticleFlowJet04_Beta[kMaxParticleFlowJet04];   //[ParticleFlowJet04_]
   Float_t         ParticleFlowJet04_BetaStar[kMaxParticleFlowJet04];   //[ParticleFlowJet04_]
   Float_t         ParticleFlowJet04_MeanSqDeltaR[kMaxParticleFlowJet04];   //[ParticleFlowJet04_]
   Float_t         ParticleFlowJet04_PTD[kMaxParticleFlowJet04];   //[ParticleFlowJet04_]
   Float_t         ParticleFlowJet04_FracPt[kMaxParticleFlowJet04][5];   //[ParticleFlowJet04_]
   Float_t         ParticleFlowJet04_Tau[kMaxParticleFlowJet04][5];   //[ParticleFlowJet04_]
   TLorentzVector  ParticleFlowJet04_SoftDroppedJet[kMaxParticleFlowJet04];
   TLorentzVector  ParticleFlowJet04_SoftDroppedSubJet1[kMaxParticleFlowJet04];
   TLorentzVector  ParticleFlowJet04_SoftDroppedSubJet2[kMaxParticleFlowJet04];
   TLorentzVector  ParticleFlowJet04_TrimmedP4[5][kMaxParticleFlowJet04];
   TLorentzVector  ParticleFlowJet04_PrunedP4[5][kMaxParticleFlowJet04];
   TLorentzVector  ParticleFlowJet04_SoftDroppedP4[5][kMaxParticleFlowJet04];
   Int_t           ParticleFlowJet04_NSubJetsTrimmed[kMaxParticleFlowJet04];   //[ParticleFlowJet04_]
   Int_t           ParticleFlowJet04_NSubJetsPruned[kMaxParticleFlowJet04];   //[ParticleFlowJet04_]
   Int_t           ParticleFlowJet04_NSubJetsSoftDropped[kMaxParticleFlowJet04];   //[ParticleFlowJet04_]
   Double_t        ParticleFlowJet04_ExclYmerge12[kMaxParticleFlowJet04];   //[ParticleFlowJet04_]
   Double_t        ParticleFlowJet04_ExclYmerge23[kMaxParticleFlowJet04];   //[ParticleFlowJet04_]
   Double_t        ParticleFlowJet04_ExclYmerge34[kMaxParticleFlowJet04];   //[ParticleFlowJet04_]
   Double_t        ParticleFlowJet04_ExclYmerge45[kMaxParticleFlowJet04];   //[ParticleFlowJet04_]
   Double_t        ParticleFlowJet04_ExclYmerge56[kMaxParticleFlowJet04];   //[ParticleFlowJet04_]
   TRefArray       ParticleFlowJet04_Constituents[kMaxParticleFlowJet04];
   TRefArray       ParticleFlowJet04_Particles[kMaxParticleFlowJet04];
   TLorentzVector  ParticleFlowJet04_Area[kMaxParticleFlowJet04];
   Int_t           ParticleFlowJet04_size;
   Int_t           ParticleFlowJet10_;
   UInt_t          ParticleFlowJet10_fUniqueID[kMaxParticleFlowJet10];   //[ParticleFlowJet10_]
   UInt_t          ParticleFlowJet10_fBits[kMaxParticleFlowJet10];   //[ParticleFlowJet10_]
   Float_t         ParticleFlowJet10_PT[kMaxParticleFlowJet10];   //[ParticleFlowJet10_]
   Float_t         ParticleFlowJet10_Eta[kMaxParticleFlowJet10];   //[ParticleFlowJet10_]
   Float_t         ParticleFlowJet10_Phi[kMaxParticleFlowJet10];   //[ParticleFlowJet10_]
   Float_t         ParticleFlowJet10_T[kMaxParticleFlowJet10];   //[ParticleFlowJet10_]
   Float_t         ParticleFlowJet10_Mass[kMaxParticleFlowJet10];   //[ParticleFlowJet10_]
   Float_t         ParticleFlowJet10_DeltaEta[kMaxParticleFlowJet10];   //[ParticleFlowJet10_]
   Float_t         ParticleFlowJet10_DeltaPhi[kMaxParticleFlowJet10];   //[ParticleFlowJet10_]
   UInt_t          ParticleFlowJet10_Flavor[kMaxParticleFlowJet10];   //[ParticleFlowJet10_]
   UInt_t          ParticleFlowJet10_FlavorAlgo[kMaxParticleFlowJet10];   //[ParticleFlowJet10_]
   UInt_t          ParticleFlowJet10_FlavorPhys[kMaxParticleFlowJet10];   //[ParticleFlowJet10_]
   UInt_t          ParticleFlowJet10_TauFlavor[kMaxParticleFlowJet10];   //[ParticleFlowJet10_]
   UInt_t          ParticleFlowJet10_BTag[kMaxParticleFlowJet10];   //[ParticleFlowJet10_]
   UInt_t          ParticleFlowJet10_BTagAlgo[kMaxParticleFlowJet10];   //[ParticleFlowJet10_]
   UInt_t          ParticleFlowJet10_BTagPhys[kMaxParticleFlowJet10];   //[ParticleFlowJet10_]
   UInt_t          ParticleFlowJet10_TauTag[kMaxParticleFlowJet10];   //[ParticleFlowJet10_]
   Float_t         ParticleFlowJet10_TauWeight[kMaxParticleFlowJet10];   //[ParticleFlowJet10_]
   Int_t           ParticleFlowJet10_Charge[kMaxParticleFlowJet10];   //[ParticleFlowJet10_]
   Float_t         ParticleFlowJet10_EhadOverEem[kMaxParticleFlowJet10];   //[ParticleFlowJet10_]
   Int_t           ParticleFlowJet10_NCharged[kMaxParticleFlowJet10];   //[ParticleFlowJet10_]
   Int_t           ParticleFlowJet10_NNeutrals[kMaxParticleFlowJet10];   //[ParticleFlowJet10_]
   Float_t         ParticleFlowJet10_NeutralEnergyFraction[kMaxParticleFlowJet10];   //[ParticleFlowJet10_]
   Float_t         ParticleFlowJet10_ChargedEnergyFraction[kMaxParticleFlowJet10];   //[ParticleFlowJet10_]
   Float_t         ParticleFlowJet10_Beta[kMaxParticleFlowJet10];   //[ParticleFlowJet10_]
   Float_t         ParticleFlowJet10_BetaStar[kMaxParticleFlowJet10];   //[ParticleFlowJet10_]
   Float_t         ParticleFlowJet10_MeanSqDeltaR[kMaxParticleFlowJet10];   //[ParticleFlowJet10_]
   Float_t         ParticleFlowJet10_PTD[kMaxParticleFlowJet10];   //[ParticleFlowJet10_]
   Float_t         ParticleFlowJet10_FracPt[kMaxParticleFlowJet10][5];   //[ParticleFlowJet10_]
   Float_t         ParticleFlowJet10_Tau[kMaxParticleFlowJet10][5];   //[ParticleFlowJet10_]
   TLorentzVector  ParticleFlowJet10_SoftDroppedJet[kMaxParticleFlowJet10];
   TLorentzVector  ParticleFlowJet10_SoftDroppedSubJet1[kMaxParticleFlowJet10];
   TLorentzVector  ParticleFlowJet10_SoftDroppedSubJet2[kMaxParticleFlowJet10];
   TLorentzVector  ParticleFlowJet10_TrimmedP4[5][kMaxParticleFlowJet10];
   TLorentzVector  ParticleFlowJet10_PrunedP4[5][kMaxParticleFlowJet10];
   TLorentzVector  ParticleFlowJet10_SoftDroppedP4[5][kMaxParticleFlowJet10];
   Int_t           ParticleFlowJet10_NSubJetsTrimmed[kMaxParticleFlowJet10];   //[ParticleFlowJet10_]
   Int_t           ParticleFlowJet10_NSubJetsPruned[kMaxParticleFlowJet10];   //[ParticleFlowJet10_]
   Int_t           ParticleFlowJet10_NSubJetsSoftDropped[kMaxParticleFlowJet10];   //[ParticleFlowJet10_]
   Double_t        ParticleFlowJet10_ExclYmerge12[kMaxParticleFlowJet10];   //[ParticleFlowJet10_]
   Double_t        ParticleFlowJet10_ExclYmerge23[kMaxParticleFlowJet10];   //[ParticleFlowJet10_]
   Double_t        ParticleFlowJet10_ExclYmerge34[kMaxParticleFlowJet10];   //[ParticleFlowJet10_]
   Double_t        ParticleFlowJet10_ExclYmerge45[kMaxParticleFlowJet10];   //[ParticleFlowJet10_]
   Double_t        ParticleFlowJet10_ExclYmerge56[kMaxParticleFlowJet10];   //[ParticleFlowJet10_]
   TRefArray       ParticleFlowJet10_Constituents[kMaxParticleFlowJet10];
   TRefArray       ParticleFlowJet10_Particles[kMaxParticleFlowJet10];
   TLorentzVector  ParticleFlowJet10_Area[kMaxParticleFlowJet10];
   Int_t           ParticleFlowJet10_size;
   Int_t           GenMissingET_;
   UInt_t          GenMissingET_fUniqueID[kMaxGenMissingET];   //[GenMissingET_]
   UInt_t          GenMissingET_fBits[kMaxGenMissingET];   //[GenMissingET_]
   Float_t         GenMissingET_MET[kMaxGenMissingET];   //[GenMissingET_]
   Float_t         GenMissingET_Eta[kMaxGenMissingET];   //[GenMissingET_]
   Float_t         GenMissingET_Phi[kMaxGenMissingET];   //[GenMissingET_]
   Int_t           GenMissingET_size;
   Int_t           Jet_;
   UInt_t          Jet_fUniqueID[kMaxJet];   //[Jet_]
   UInt_t          Jet_fBits[kMaxJet];   //[Jet_]
   Float_t         Jet_PT[kMaxJet];   //[Jet_]
   Float_t         Jet_Eta[kMaxJet];   //[Jet_]
   Float_t         Jet_Phi[kMaxJet];   //[Jet_]
   Float_t         Jet_T[kMaxJet];   //[Jet_]
   Float_t         Jet_Mass[kMaxJet];   //[Jet_]
   Float_t         Jet_DeltaEta[kMaxJet];   //[Jet_]
   Float_t         Jet_DeltaPhi[kMaxJet];   //[Jet_]
   UInt_t          Jet_Flavor[kMaxJet];   //[Jet_]
   UInt_t          Jet_FlavorAlgo[kMaxJet];   //[Jet_]
   UInt_t          Jet_FlavorPhys[kMaxJet];   //[Jet_]
   UInt_t          Jet_TauFlavor[kMaxJet];   //[Jet_]
   UInt_t          Jet_BTag[kMaxJet];   //[Jet_]
   UInt_t          Jet_BTagAlgo[kMaxJet];   //[Jet_]
   UInt_t          Jet_BTagPhys[kMaxJet];   //[Jet_]
   UInt_t          Jet_TauTag[kMaxJet];   //[Jet_]
   Float_t         Jet_TauWeight[kMaxJet];   //[Jet_]
   Int_t           Jet_Charge[kMaxJet];   //[Jet_]
   Float_t         Jet_EhadOverEem[kMaxJet];   //[Jet_]
   Int_t           Jet_NCharged[kMaxJet];   //[Jet_]
   Int_t           Jet_NNeutrals[kMaxJet];   //[Jet_]
   Float_t         Jet_NeutralEnergyFraction[kMaxJet];   //[Jet_]
   Float_t         Jet_ChargedEnergyFraction[kMaxJet];   //[Jet_]
   Float_t         Jet_Beta[kMaxJet];   //[Jet_]
   Float_t         Jet_BetaStar[kMaxJet];   //[Jet_]
   Float_t         Jet_MeanSqDeltaR[kMaxJet];   //[Jet_]
   Float_t         Jet_PTD[kMaxJet];   //[Jet_]
   Float_t         Jet_FracPt[kMaxJet][5];   //[Jet_]
   Float_t         Jet_Tau[kMaxJet][5];   //[Jet_]
   TLorentzVector  Jet_SoftDroppedJet[kMaxJet];
   TLorentzVector  Jet_SoftDroppedSubJet1[kMaxJet];
   TLorentzVector  Jet_SoftDroppedSubJet2[kMaxJet];
   TLorentzVector  Jet_TrimmedP4[5][kMaxJet];
   TLorentzVector  Jet_PrunedP4[5][kMaxJet];
   TLorentzVector  Jet_SoftDroppedP4[5][kMaxJet];
   Int_t           Jet_NSubJetsTrimmed[kMaxJet];   //[Jet_]
   Int_t           Jet_NSubJetsPruned[kMaxJet];   //[Jet_]
   Int_t           Jet_NSubJetsSoftDropped[kMaxJet];   //[Jet_]
   Double_t        Jet_ExclYmerge12[kMaxJet];   //[Jet_]
   Double_t        Jet_ExclYmerge23[kMaxJet];   //[Jet_]
   Double_t        Jet_ExclYmerge34[kMaxJet];   //[Jet_]
   Double_t        Jet_ExclYmerge45[kMaxJet];   //[Jet_]
   Double_t        Jet_ExclYmerge56[kMaxJet];   //[Jet_]
   TRefArray       Jet_Constituents[kMaxJet];
   TRefArray       Jet_Particles[kMaxJet];
   TLorentzVector  Jet_Area[kMaxJet];
   Int_t           Jet_size;
   Int_t           Electron_;
   UInt_t          Electron_fUniqueID[kMaxElectron];   //[Electron_]
   UInt_t          Electron_fBits[kMaxElectron];   //[Electron_]
   Float_t         Electron_PT[kMaxElectron];   //[Electron_]
   Float_t         Electron_Eta[kMaxElectron];   //[Electron_]
   Float_t         Electron_Phi[kMaxElectron];   //[Electron_]
   Float_t         Electron_T[kMaxElectron];   //[Electron_]
   Int_t           Electron_Charge[kMaxElectron];   //[Electron_]
   Float_t         Electron_EhadOverEem[kMaxElectron];   //[Electron_]
   TRef            Electron_Particle[kMaxElectron];
   Float_t         Electron_IsolationVar[kMaxElectron];   //[Electron_]
   Float_t         Electron_IsolationVarRhoCorr[kMaxElectron];   //[Electron_]
   Float_t         Electron_SumPtCharged[kMaxElectron];   //[Electron_]
   Float_t         Electron_SumPtNeutral[kMaxElectron];   //[Electron_]
   Float_t         Electron_SumPtChargedPU[kMaxElectron];   //[Electron_]
   Float_t         Electron_SumPt[kMaxElectron];   //[Electron_]
   Float_t         Electron_D0[kMaxElectron];   //[Electron_]
   Float_t         Electron_DZ[kMaxElectron];   //[Electron_]
   Float_t         Electron_ErrorD0[kMaxElectron];   //[Electron_]
   Float_t         Electron_ErrorDZ[kMaxElectron];   //[Electron_]
   Int_t           Electron_size;
   Int_t           Photon_;
   UInt_t          Photon_fUniqueID[kMaxPhoton];   //[Photon_]
   UInt_t          Photon_fBits[kMaxPhoton];   //[Photon_]
   Float_t         Photon_PT[kMaxPhoton];   //[Photon_]
   Float_t         Photon_Eta[kMaxPhoton];   //[Photon_]
   Float_t         Photon_Phi[kMaxPhoton];   //[Photon_]
   Float_t         Photon_E[kMaxPhoton];   //[Photon_]
   Float_t         Photon_T[kMaxPhoton];   //[Photon_]
   Float_t         Photon_EhadOverEem[kMaxPhoton];   //[Photon_]
   TRefArray       Photon_Particles[kMaxPhoton];
   Float_t         Photon_IsolationVar[kMaxPhoton];   //[Photon_]
   Float_t         Photon_IsolationVarRhoCorr[kMaxPhoton];   //[Photon_]
   Float_t         Photon_SumPtCharged[kMaxPhoton];   //[Photon_]
   Float_t         Photon_SumPtNeutral[kMaxPhoton];   //[Photon_]
   Float_t         Photon_SumPtChargedPU[kMaxPhoton];   //[Photon_]
   Float_t         Photon_SumPt[kMaxPhoton];   //[Photon_]
   Int_t           Photon_Status[kMaxPhoton];   //[Photon_]
   Int_t           Photon_size;
   Int_t           Muon_;
   UInt_t          Muon_fUniqueID[kMaxMuon];   //[Muon_]
   UInt_t          Muon_fBits[kMaxMuon];   //[Muon_]
   Float_t         Muon_PT[kMaxMuon];   //[Muon_]
   Float_t         Muon_Eta[kMaxMuon];   //[Muon_]
   Float_t         Muon_Phi[kMaxMuon];   //[Muon_]
   Float_t         Muon_T[kMaxMuon];   //[Muon_]
   Int_t           Muon_Charge[kMaxMuon];   //[Muon_]
   TRef            Muon_Particle[kMaxMuon];
   Float_t         Muon_IsolationVar[kMaxMuon];   //[Muon_]
   Float_t         Muon_IsolationVarRhoCorr[kMaxMuon];   //[Muon_]
   Float_t         Muon_SumPtCharged[kMaxMuon];   //[Muon_]
   Float_t         Muon_SumPtNeutral[kMaxMuon];   //[Muon_]
   Float_t         Muon_SumPtChargedPU[kMaxMuon];   //[Muon_]
   Float_t         Muon_SumPt[kMaxMuon];   //[Muon_]
   Float_t         Muon_D0[kMaxMuon];   //[Muon_]
   Float_t         Muon_DZ[kMaxMuon];   //[Muon_]
   Float_t         Muon_ErrorD0[kMaxMuon];   //[Muon_]
   Float_t         Muon_ErrorDZ[kMaxMuon];   //[Muon_]
   Int_t           Muon_size;
   Int_t           MissingET_;
   UInt_t          MissingET_fUniqueID[kMaxMissingET];   //[MissingET_]
   UInt_t          MissingET_fBits[kMaxMissingET];   //[MissingET_]
   Float_t         MissingET_MET[kMaxMissingET];   //[MissingET_]
   Float_t         MissingET_Eta[kMaxMissingET];   //[MissingET_]
   Float_t         MissingET_Phi[kMaxMissingET];   //[MissingET_]
   Int_t           MissingET_size;
   Int_t           ScalarHT_;
   UInt_t          ScalarHT_fUniqueID[kMaxScalarHT];   //[ScalarHT_]
   UInt_t          ScalarHT_fBits[kMaxScalarHT];   //[ScalarHT_]
   Float_t         ScalarHT_HT[kMaxScalarHT];   //[ScalarHT_]
   Int_t           ScalarHT_size;

   // List of branches
   TBranch        *b_Event_;   //!
   TBranch        *b_Event_fUniqueID;   //!
   TBranch        *b_Event_fBits;   //!
   TBranch        *b_Event_Number;   //!
   TBranch        *b_Event_ReadTime;   //!
   TBranch        *b_Event_ProcTime;   //!
   TBranch        *b_Event_ProcessID;   //!
   TBranch        *b_Event_MPI;   //!
   TBranch        *b_Event_Weight;   //!
   TBranch        *b_Event_CrossSection;   //!
   TBranch        *b_Event_CrossSectionError;   //!
   TBranch        *b_Event_Scale;   //!
   TBranch        *b_Event_AlphaQED;   //!
   TBranch        *b_Event_AlphaQCD;   //!
   TBranch        *b_Event_ID1;   //!
   TBranch        *b_Event_ID2;   //!
   TBranch        *b_Event_X1;   //!
   TBranch        *b_Event_X2;   //!
   TBranch        *b_Event_ScalePDF;   //!
   TBranch        *b_Event_PDF1;   //!
   TBranch        *b_Event_PDF2;   //!
   TBranch        *b_Event_size;   //!
   TBranch        *b_Weight_;   //!
   TBranch        *b_Weight_fUniqueID;   //!
   TBranch        *b_Weight_fBits;   //!
   TBranch        *b_Weight_Weight;   //!
   TBranch        *b_Weight_size;   //!
   TBranch        *b_Particle_;   //!
   TBranch        *b_Particle_fUniqueID;   //!
   TBranch        *b_Particle_fBits;   //!
   TBranch        *b_Particle_PID;   //!
   TBranch        *b_Particle_Status;   //!
   TBranch        *b_Particle_IsPU;   //!
   TBranch        *b_Particle_M1;   //!
   TBranch        *b_Particle_M2;   //!
   TBranch        *b_Particle_D1;   //!
   TBranch        *b_Particle_D2;   //!
   TBranch        *b_Particle_Charge;   //!
   TBranch        *b_Particle_Mass;   //!
   TBranch        *b_Particle_E;   //!
   TBranch        *b_Particle_Px;   //!
   TBranch        *b_Particle_Py;   //!
   TBranch        *b_Particle_Pz;   //!
   TBranch        *b_Particle_P;   //!
   TBranch        *b_Particle_PT;   //!
   TBranch        *b_Particle_Eta;   //!
   TBranch        *b_Particle_Phi;   //!
   TBranch        *b_Particle_Rapidity;   //!
   TBranch        *b_Particle_T;   //!
   TBranch        *b_Particle_X;   //!
   TBranch        *b_Particle_Y;   //!
   TBranch        *b_Particle_Z;   //!
   TBranch        *b_Particle_size;   //!
   TBranch        *b_Track_;   //!
   TBranch        *b_Track_fUniqueID;   //!
   TBranch        *b_Track_fBits;   //!
   TBranch        *b_Track_PID;   //!
   TBranch        *b_Track_Charge;   //!
   TBranch        *b_Track_P;   //!
   TBranch        *b_Track_PT;   //!
   TBranch        *b_Track_Eta;   //!
   TBranch        *b_Track_Phi;   //!
   TBranch        *b_Track_CtgTheta;   //!
   TBranch        *b_Track_C;   //!
   TBranch        *b_Track_Mass;   //!
   TBranch        *b_Track_EtaOuter;   //!
   TBranch        *b_Track_PhiOuter;   //!
   TBranch        *b_Track_T;   //!
   TBranch        *b_Track_X;   //!
   TBranch        *b_Track_Y;   //!
   TBranch        *b_Track_Z;   //!
   TBranch        *b_Track_TOuter;   //!
   TBranch        *b_Track_XOuter;   //!
   TBranch        *b_Track_YOuter;   //!
   TBranch        *b_Track_ZOuter;   //!
   TBranch        *b_Track_Xd;   //!
   TBranch        *b_Track_Yd;   //!
   TBranch        *b_Track_Zd;   //!
   TBranch        *b_Track_XFirstHit;   //!
   TBranch        *b_Track_YFirstHit;   //!
   TBranch        *b_Track_ZFirstHit;   //!
   TBranch        *b_Track_L;   //!
   TBranch        *b_Track_D0;   //!
   TBranch        *b_Track_DZ;   //!
   TBranch        *b_Track_Nclusters;   //!
   TBranch        *b_Track_dNdx;   //!
   TBranch        *b_Track_ErrorP;   //!
   TBranch        *b_Track_ErrorPT;   //!
   TBranch        *b_Track_ErrorPhi;   //!
   TBranch        *b_Track_ErrorCtgTheta;   //!
   TBranch        *b_Track_ErrorT;   //!
   TBranch        *b_Track_ErrorD0;   //!
   TBranch        *b_Track_ErrorDZ;   //!
   TBranch        *b_Track_ErrorC;   //!
   TBranch        *b_Track_ErrorD0Phi;   //!
   TBranch        *b_Track_ErrorD0C;   //!
   TBranch        *b_Track_ErrorD0DZ;   //!
   TBranch        *b_Track_ErrorD0CtgTheta;   //!
   TBranch        *b_Track_ErrorPhiC;   //!
   TBranch        *b_Track_ErrorPhiDZ;   //!
   TBranch        *b_Track_ErrorPhiCtgTheta;   //!
   TBranch        *b_Track_ErrorCDZ;   //!
   TBranch        *b_Track_ErrorCCtgTheta;   //!
   TBranch        *b_Track_ErrorDZCtgTheta;   //!
   TBranch        *b_Track_Particle;   //!
   TBranch        *b_Track_VertexIndex;   //!
   TBranch        *b_Track_size;   //!
   TBranch        *b_Tower_;   //!
   TBranch        *b_Tower_fUniqueID;   //!
   TBranch        *b_Tower_fBits;   //!
   TBranch        *b_Tower_ET;   //!
   TBranch        *b_Tower_Eta;   //!
   TBranch        *b_Tower_Phi;   //!
   TBranch        *b_Tower_E;   //!
   TBranch        *b_Tower_T;   //!
   TBranch        *b_Tower_X;   //!
   TBranch        *b_Tower_Y;   //!
   TBranch        *b_Tower_Z;   //!
   TBranch        *b_Tower_NTimeHits;   //!
   TBranch        *b_Tower_Eem;   //!
   TBranch        *b_Tower_Ehad;   //!
   TBranch        *b_Tower_Etrk;   //!
   TBranch        *b_Tower_Edges;   //!
   TBranch        *b_Tower_Particles;   //!
   TBranch        *b_Tower_size;   //!
   TBranch        *b_EFlowTrack_;   //!
   TBranch        *b_EFlowTrack_fUniqueID;   //!
   TBranch        *b_EFlowTrack_fBits;   //!
   TBranch        *b_EFlowTrack_PID;   //!
   TBranch        *b_EFlowTrack_Charge;   //!
   TBranch        *b_EFlowTrack_P;   //!
   TBranch        *b_EFlowTrack_PT;   //!
   TBranch        *b_EFlowTrack_Eta;   //!
   TBranch        *b_EFlowTrack_Phi;   //!
   TBranch        *b_EFlowTrack_CtgTheta;   //!
   TBranch        *b_EFlowTrack_C;   //!
   TBranch        *b_EFlowTrack_Mass;   //!
   TBranch        *b_EFlowTrack_EtaOuter;   //!
   TBranch        *b_EFlowTrack_PhiOuter;   //!
   TBranch        *b_EFlowTrack_T;   //!
   TBranch        *b_EFlowTrack_X;   //!
   TBranch        *b_EFlowTrack_Y;   //!
   TBranch        *b_EFlowTrack_Z;   //!
   TBranch        *b_EFlowTrack_TOuter;   //!
   TBranch        *b_EFlowTrack_XOuter;   //!
   TBranch        *b_EFlowTrack_YOuter;   //!
   TBranch        *b_EFlowTrack_ZOuter;   //!
   TBranch        *b_EFlowTrack_Xd;   //!
   TBranch        *b_EFlowTrack_Yd;   //!
   TBranch        *b_EFlowTrack_Zd;   //!
   TBranch        *b_EFlowTrack_XFirstHit;   //!
   TBranch        *b_EFlowTrack_YFirstHit;   //!
   TBranch        *b_EFlowTrack_ZFirstHit;   //!
   TBranch        *b_EFlowTrack_L;   //!
   TBranch        *b_EFlowTrack_D0;   //!
   TBranch        *b_EFlowTrack_DZ;   //!
   TBranch        *b_EFlowTrack_Nclusters;   //!
   TBranch        *b_EFlowTrack_dNdx;   //!
   TBranch        *b_EFlowTrack_ErrorP;   //!
   TBranch        *b_EFlowTrack_ErrorPT;   //!
   TBranch        *b_EFlowTrack_ErrorPhi;   //!
   TBranch        *b_EFlowTrack_ErrorCtgTheta;   //!
   TBranch        *b_EFlowTrack_ErrorT;   //!
   TBranch        *b_EFlowTrack_ErrorD0;   //!
   TBranch        *b_EFlowTrack_ErrorDZ;   //!
   TBranch        *b_EFlowTrack_ErrorC;   //!
   TBranch        *b_EFlowTrack_ErrorD0Phi;   //!
   TBranch        *b_EFlowTrack_ErrorD0C;   //!
   TBranch        *b_EFlowTrack_ErrorD0DZ;   //!
   TBranch        *b_EFlowTrack_ErrorD0CtgTheta;   //!
   TBranch        *b_EFlowTrack_ErrorPhiC;   //!
   TBranch        *b_EFlowTrack_ErrorPhiDZ;   //!
   TBranch        *b_EFlowTrack_ErrorPhiCtgTheta;   //!
   TBranch        *b_EFlowTrack_ErrorCDZ;   //!
   TBranch        *b_EFlowTrack_ErrorCCtgTheta;   //!
   TBranch        *b_EFlowTrack_ErrorDZCtgTheta;   //!
   TBranch        *b_EFlowTrack_Particle;   //!
   TBranch        *b_EFlowTrack_VertexIndex;   //!
   TBranch        *b_EFlowTrack_size;   //!
   TBranch        *b_EFlowPhoton_;   //!
   TBranch        *b_EFlowPhoton_fUniqueID;   //!
   TBranch        *b_EFlowPhoton_fBits;   //!
   TBranch        *b_EFlowPhoton_ET;   //!
   TBranch        *b_EFlowPhoton_Eta;   //!
   TBranch        *b_EFlowPhoton_Phi;   //!
   TBranch        *b_EFlowPhoton_E;   //!
   TBranch        *b_EFlowPhoton_T;   //!
   TBranch        *b_EFlowPhoton_X;   //!
   TBranch        *b_EFlowPhoton_Y;   //!
   TBranch        *b_EFlowPhoton_Z;   //!
   TBranch        *b_EFlowPhoton_NTimeHits;   //!
   TBranch        *b_EFlowPhoton_Eem;   //!
   TBranch        *b_EFlowPhoton_Ehad;   //!
   TBranch        *b_EFlowPhoton_Etrk;   //!
   TBranch        *b_EFlowPhoton_Edges;   //!
   TBranch        *b_EFlowPhoton_Particles;   //!
   TBranch        *b_EFlowPhoton_size;   //!
   TBranch        *b_EFlowNeutralHadron_;   //!
   TBranch        *b_EFlowNeutralHadron_fUniqueID;   //!
   TBranch        *b_EFlowNeutralHadron_fBits;   //!
   TBranch        *b_EFlowNeutralHadron_ET;   //!
   TBranch        *b_EFlowNeutralHadron_Eta;   //!
   TBranch        *b_EFlowNeutralHadron_Phi;   //!
   TBranch        *b_EFlowNeutralHadron_E;   //!
   TBranch        *b_EFlowNeutralHadron_T;   //!
   TBranch        *b_EFlowNeutralHadron_X;   //!
   TBranch        *b_EFlowNeutralHadron_Y;   //!
   TBranch        *b_EFlowNeutralHadron_Z;   //!
   TBranch        *b_EFlowNeutralHadron_NTimeHits;   //!
   TBranch        *b_EFlowNeutralHadron_Eem;   //!
   TBranch        *b_EFlowNeutralHadron_Ehad;   //!
   TBranch        *b_EFlowNeutralHadron_Etrk;   //!
   TBranch        *b_EFlowNeutralHadron_Edges;   //!
   TBranch        *b_EFlowNeutralHadron_Particles;   //!
   TBranch        *b_EFlowNeutralHadron_size;   //!
   TBranch        *b_GenJet04_;   //!
   TBranch        *b_GenJet04_fUniqueID;   //!
   TBranch        *b_GenJet04_fBits;   //!
   TBranch        *b_GenJet04_PT;   //!
   TBranch        *b_GenJet04_Eta;   //!
   TBranch        *b_GenJet04_Phi;   //!
   TBranch        *b_GenJet04_T;   //!
   TBranch        *b_GenJet04_Mass;   //!
   TBranch        *b_GenJet04_DeltaEta;   //!
   TBranch        *b_GenJet04_DeltaPhi;   //!
   TBranch        *b_GenJet04_Flavor;   //!
   TBranch        *b_GenJet04_FlavorAlgo;   //!
   TBranch        *b_GenJet04_FlavorPhys;   //!
   TBranch        *b_GenJet04_TauFlavor;   //!
   TBranch        *b_GenJet04_BTag;   //!
   TBranch        *b_GenJet04_BTagAlgo;   //!
   TBranch        *b_GenJet04_BTagPhys;   //!
   TBranch        *b_GenJet04_TauTag;   //!
   TBranch        *b_GenJet04_TauWeight;   //!
   TBranch        *b_GenJet04_Charge;   //!
   TBranch        *b_GenJet04_EhadOverEem;   //!
   TBranch        *b_GenJet04_NCharged;   //!
   TBranch        *b_GenJet04_NNeutrals;   //!
   TBranch        *b_GenJet04_NeutralEnergyFraction;   //!
   TBranch        *b_GenJet04_ChargedEnergyFraction;   //!
   TBranch        *b_GenJet04_Beta;   //!
   TBranch        *b_GenJet04_BetaStar;   //!
   TBranch        *b_GenJet04_MeanSqDeltaR;   //!
   TBranch        *b_GenJet04_PTD;   //!
   TBranch        *b_GenJet04_FracPt;   //!
   TBranch        *b_GenJet04_Tau;   //!
   TBranch        *b_GenJet04_SoftDroppedJet;   //!
   TBranch        *b_GenJet04_SoftDroppedSubJet1;   //!
   TBranch        *b_GenJet04_SoftDroppedSubJet2;   //!
   TBranch        *b_GenJet04_TrimmedP4;   //!
   TBranch        *b_GenJet04_PrunedP4;   //!
   TBranch        *b_GenJet04_SoftDroppedP4;   //!
   TBranch        *b_GenJet04_NSubJetsTrimmed;   //!
   TBranch        *b_GenJet04_NSubJetsPruned;   //!
   TBranch        *b_GenJet04_NSubJetsSoftDropped;   //!
   TBranch        *b_GenJet04_ExclYmerge12;   //!
   TBranch        *b_GenJet04_ExclYmerge23;   //!
   TBranch        *b_GenJet04_ExclYmerge34;   //!
   TBranch        *b_GenJet04_ExclYmerge45;   //!
   TBranch        *b_GenJet04_ExclYmerge56;   //!
   TBranch        *b_GenJet04_Constituents;   //!
   TBranch        *b_GenJet04_Particles;   //!
   TBranch        *b_GenJet04_Area;   //!
   TBranch        *b_GenJet04_size;   //!
   TBranch        *b_GenJet10_;   //!
   TBranch        *b_GenJet10_fUniqueID;   //!
   TBranch        *b_GenJet10_fBits;   //!
   TBranch        *b_GenJet10_PT;   //!
   TBranch        *b_GenJet10_Eta;   //!
   TBranch        *b_GenJet10_Phi;   //!
   TBranch        *b_GenJet10_T;   //!
   TBranch        *b_GenJet10_Mass;   //!
   TBranch        *b_GenJet10_DeltaEta;   //!
   TBranch        *b_GenJet10_DeltaPhi;   //!
   TBranch        *b_GenJet10_Flavor;   //!
   TBranch        *b_GenJet10_FlavorAlgo;   //!
   TBranch        *b_GenJet10_FlavorPhys;   //!
   TBranch        *b_GenJet10_TauFlavor;   //!
   TBranch        *b_GenJet10_BTag;   //!
   TBranch        *b_GenJet10_BTagAlgo;   //!
   TBranch        *b_GenJet10_BTagPhys;   //!
   TBranch        *b_GenJet10_TauTag;   //!
   TBranch        *b_GenJet10_TauWeight;   //!
   TBranch        *b_GenJet10_Charge;   //!
   TBranch        *b_GenJet10_EhadOverEem;   //!
   TBranch        *b_GenJet10_NCharged;   //!
   TBranch        *b_GenJet10_NNeutrals;   //!
   TBranch        *b_GenJet10_NeutralEnergyFraction;   //!
   TBranch        *b_GenJet10_ChargedEnergyFraction;   //!
   TBranch        *b_GenJet10_Beta;   //!
   TBranch        *b_GenJet10_BetaStar;   //!
   TBranch        *b_GenJet10_MeanSqDeltaR;   //!
   TBranch        *b_GenJet10_PTD;   //!
   TBranch        *b_GenJet10_FracPt;   //!
   TBranch        *b_GenJet10_Tau;   //!
   TBranch        *b_GenJet10_SoftDroppedJet;   //!
   TBranch        *b_GenJet10_SoftDroppedSubJet1;   //!
   TBranch        *b_GenJet10_SoftDroppedSubJet2;   //!
   TBranch        *b_GenJet10_TrimmedP4;   //!
   TBranch        *b_GenJet10_PrunedP4;   //!
   TBranch        *b_GenJet10_SoftDroppedP4;   //!
   TBranch        *b_GenJet10_NSubJetsTrimmed;   //!
   TBranch        *b_GenJet10_NSubJetsPruned;   //!
   TBranch        *b_GenJet10_NSubJetsSoftDropped;   //!
   TBranch        *b_GenJet10_ExclYmerge12;   //!
   TBranch        *b_GenJet10_ExclYmerge23;   //!
   TBranch        *b_GenJet10_ExclYmerge34;   //!
   TBranch        *b_GenJet10_ExclYmerge45;   //!
   TBranch        *b_GenJet10_ExclYmerge56;   //!
   TBranch        *b_GenJet10_Constituents;   //!
   TBranch        *b_GenJet10_Particles;   //!
   TBranch        *b_GenJet10_Area;   //!
   TBranch        *b_GenJet10_size;   //!
   TBranch        *b_CaloJet04_;   //!
   TBranch        *b_CaloJet04_fUniqueID;   //!
   TBranch        *b_CaloJet04_fBits;   //!
   TBranch        *b_CaloJet04_PT;   //!
   TBranch        *b_CaloJet04_Eta;   //!
   TBranch        *b_CaloJet04_Phi;   //!
   TBranch        *b_CaloJet04_T;   //!
   TBranch        *b_CaloJet04_Mass;   //!
   TBranch        *b_CaloJet04_DeltaEta;   //!
   TBranch        *b_CaloJet04_DeltaPhi;   //!
   TBranch        *b_CaloJet04_Flavor;   //!
   TBranch        *b_CaloJet04_FlavorAlgo;   //!
   TBranch        *b_CaloJet04_FlavorPhys;   //!
   TBranch        *b_CaloJet04_TauFlavor;   //!
   TBranch        *b_CaloJet04_BTag;   //!
   TBranch        *b_CaloJet04_BTagAlgo;   //!
   TBranch        *b_CaloJet04_BTagPhys;   //!
   TBranch        *b_CaloJet04_TauTag;   //!
   TBranch        *b_CaloJet04_TauWeight;   //!
   TBranch        *b_CaloJet04_Charge;   //!
   TBranch        *b_CaloJet04_EhadOverEem;   //!
   TBranch        *b_CaloJet04_NCharged;   //!
   TBranch        *b_CaloJet04_NNeutrals;   //!
   TBranch        *b_CaloJet04_NeutralEnergyFraction;   //!
   TBranch        *b_CaloJet04_ChargedEnergyFraction;   //!
   TBranch        *b_CaloJet04_Beta;   //!
   TBranch        *b_CaloJet04_BetaStar;   //!
   TBranch        *b_CaloJet04_MeanSqDeltaR;   //!
   TBranch        *b_CaloJet04_PTD;   //!
   TBranch        *b_CaloJet04_FracPt;   //!
   TBranch        *b_CaloJet04_Tau;   //!
   TBranch        *b_CaloJet04_SoftDroppedJet;   //!
   TBranch        *b_CaloJet04_SoftDroppedSubJet1;   //!
   TBranch        *b_CaloJet04_SoftDroppedSubJet2;   //!
   TBranch        *b_CaloJet04_TrimmedP4;   //!
   TBranch        *b_CaloJet04_PrunedP4;   //!
   TBranch        *b_CaloJet04_SoftDroppedP4;   //!
   TBranch        *b_CaloJet04_NSubJetsTrimmed;   //!
   TBranch        *b_CaloJet04_NSubJetsPruned;   //!
   TBranch        *b_CaloJet04_NSubJetsSoftDropped;   //!
   TBranch        *b_CaloJet04_ExclYmerge12;   //!
   TBranch        *b_CaloJet04_ExclYmerge23;   //!
   TBranch        *b_CaloJet04_ExclYmerge34;   //!
   TBranch        *b_CaloJet04_ExclYmerge45;   //!
   TBranch        *b_CaloJet04_ExclYmerge56;   //!
   TBranch        *b_CaloJet04_Constituents;   //!
   TBranch        *b_CaloJet04_Particles;   //!
   TBranch        *b_CaloJet04_Area;   //!
   TBranch        *b_CaloJet04_size;   //!
   TBranch        *b_CaloJet10_;   //!
   TBranch        *b_CaloJet10_fUniqueID;   //!
   TBranch        *b_CaloJet10_fBits;   //!
   TBranch        *b_CaloJet10_PT;   //!
   TBranch        *b_CaloJet10_Eta;   //!
   TBranch        *b_CaloJet10_Phi;   //!
   TBranch        *b_CaloJet10_T;   //!
   TBranch        *b_CaloJet10_Mass;   //!
   TBranch        *b_CaloJet10_DeltaEta;   //!
   TBranch        *b_CaloJet10_DeltaPhi;   //!
   TBranch        *b_CaloJet10_Flavor;   //!
   TBranch        *b_CaloJet10_FlavorAlgo;   //!
   TBranch        *b_CaloJet10_FlavorPhys;   //!
   TBranch        *b_CaloJet10_TauFlavor;   //!
   TBranch        *b_CaloJet10_BTag;   //!
   TBranch        *b_CaloJet10_BTagAlgo;   //!
   TBranch        *b_CaloJet10_BTagPhys;   //!
   TBranch        *b_CaloJet10_TauTag;   //!
   TBranch        *b_CaloJet10_TauWeight;   //!
   TBranch        *b_CaloJet10_Charge;   //!
   TBranch        *b_CaloJet10_EhadOverEem;   //!
   TBranch        *b_CaloJet10_NCharged;   //!
   TBranch        *b_CaloJet10_NNeutrals;   //!
   TBranch        *b_CaloJet10_NeutralEnergyFraction;   //!
   TBranch        *b_CaloJet10_ChargedEnergyFraction;   //!
   TBranch        *b_CaloJet10_Beta;   //!
   TBranch        *b_CaloJet10_BetaStar;   //!
   TBranch        *b_CaloJet10_MeanSqDeltaR;   //!
   TBranch        *b_CaloJet10_PTD;   //!
   TBranch        *b_CaloJet10_FracPt;   //!
   TBranch        *b_CaloJet10_Tau;   //!
   TBranch        *b_CaloJet10_SoftDroppedJet;   //!
   TBranch        *b_CaloJet10_SoftDroppedSubJet1;   //!
   TBranch        *b_CaloJet10_SoftDroppedSubJet2;   //!
   TBranch        *b_CaloJet10_TrimmedP4;   //!
   TBranch        *b_CaloJet10_PrunedP4;   //!
   TBranch        *b_CaloJet10_SoftDroppedP4;   //!
   TBranch        *b_CaloJet10_NSubJetsTrimmed;   //!
   TBranch        *b_CaloJet10_NSubJetsPruned;   //!
   TBranch        *b_CaloJet10_NSubJetsSoftDropped;   //!
   TBranch        *b_CaloJet10_ExclYmerge12;   //!
   TBranch        *b_CaloJet10_ExclYmerge23;   //!
   TBranch        *b_CaloJet10_ExclYmerge34;   //!
   TBranch        *b_CaloJet10_ExclYmerge45;   //!
   TBranch        *b_CaloJet10_ExclYmerge56;   //!
   TBranch        *b_CaloJet10_Constituents;   //!
   TBranch        *b_CaloJet10_Particles;   //!
   TBranch        *b_CaloJet10_Area;   //!
   TBranch        *b_CaloJet10_size;   //!
   TBranch        *b_ParticleFlowJet04_;   //!
   TBranch        *b_ParticleFlowJet04_fUniqueID;   //!
   TBranch        *b_ParticleFlowJet04_fBits;   //!
   TBranch        *b_ParticleFlowJet04_PT;   //!
   TBranch        *b_ParticleFlowJet04_Eta;   //!
   TBranch        *b_ParticleFlowJet04_Phi;   //!
   TBranch        *b_ParticleFlowJet04_T;   //!
   TBranch        *b_ParticleFlowJet04_Mass;   //!
   TBranch        *b_ParticleFlowJet04_DeltaEta;   //!
   TBranch        *b_ParticleFlowJet04_DeltaPhi;   //!
   TBranch        *b_ParticleFlowJet04_Flavor;   //!
   TBranch        *b_ParticleFlowJet04_FlavorAlgo;   //!
   TBranch        *b_ParticleFlowJet04_FlavorPhys;   //!
   TBranch        *b_ParticleFlowJet04_TauFlavor;   //!
   TBranch        *b_ParticleFlowJet04_BTag;   //!
   TBranch        *b_ParticleFlowJet04_BTagAlgo;   //!
   TBranch        *b_ParticleFlowJet04_BTagPhys;   //!
   TBranch        *b_ParticleFlowJet04_TauTag;   //!
   TBranch        *b_ParticleFlowJet04_TauWeight;   //!
   TBranch        *b_ParticleFlowJet04_Charge;   //!
   TBranch        *b_ParticleFlowJet04_EhadOverEem;   //!
   TBranch        *b_ParticleFlowJet04_NCharged;   //!
   TBranch        *b_ParticleFlowJet04_NNeutrals;   //!
   TBranch        *b_ParticleFlowJet04_NeutralEnergyFraction;   //!
   TBranch        *b_ParticleFlowJet04_ChargedEnergyFraction;   //!
   TBranch        *b_ParticleFlowJet04_Beta;   //!
   TBranch        *b_ParticleFlowJet04_BetaStar;   //!
   TBranch        *b_ParticleFlowJet04_MeanSqDeltaR;   //!
   TBranch        *b_ParticleFlowJet04_PTD;   //!
   TBranch        *b_ParticleFlowJet04_FracPt;   //!
   TBranch        *b_ParticleFlowJet04_Tau;   //!
   TBranch        *b_ParticleFlowJet04_SoftDroppedJet;   //!
   TBranch        *b_ParticleFlowJet04_SoftDroppedSubJet1;   //!
   TBranch        *b_ParticleFlowJet04_SoftDroppedSubJet2;   //!
   TBranch        *b_ParticleFlowJet04_TrimmedP4;   //!
   TBranch        *b_ParticleFlowJet04_PrunedP4;   //!
   TBranch        *b_ParticleFlowJet04_SoftDroppedP4;   //!
   TBranch        *b_ParticleFlowJet04_NSubJetsTrimmed;   //!
   TBranch        *b_ParticleFlowJet04_NSubJetsPruned;   //!
   TBranch        *b_ParticleFlowJet04_NSubJetsSoftDropped;   //!
   TBranch        *b_ParticleFlowJet04_ExclYmerge12;   //!
   TBranch        *b_ParticleFlowJet04_ExclYmerge23;   //!
   TBranch        *b_ParticleFlowJet04_ExclYmerge34;   //!
   TBranch        *b_ParticleFlowJet04_ExclYmerge45;   //!
   TBranch        *b_ParticleFlowJet04_ExclYmerge56;   //!
   TBranch        *b_ParticleFlowJet04_Constituents;   //!
   TBranch        *b_ParticleFlowJet04_Particles;   //!
   TBranch        *b_ParticleFlowJet04_Area;   //!
   TBranch        *b_ParticleFlowJet04_size;   //!
   TBranch        *b_ParticleFlowJet10_;   //!
   TBranch        *b_ParticleFlowJet10_fUniqueID;   //!
   TBranch        *b_ParticleFlowJet10_fBits;   //!
   TBranch        *b_ParticleFlowJet10_PT;   //!
   TBranch        *b_ParticleFlowJet10_Eta;   //!
   TBranch        *b_ParticleFlowJet10_Phi;   //!
   TBranch        *b_ParticleFlowJet10_T;   //!
   TBranch        *b_ParticleFlowJet10_Mass;   //!
   TBranch        *b_ParticleFlowJet10_DeltaEta;   //!
   TBranch        *b_ParticleFlowJet10_DeltaPhi;   //!
   TBranch        *b_ParticleFlowJet10_Flavor;   //!
   TBranch        *b_ParticleFlowJet10_FlavorAlgo;   //!
   TBranch        *b_ParticleFlowJet10_FlavorPhys;   //!
   TBranch        *b_ParticleFlowJet10_TauFlavor;   //!
   TBranch        *b_ParticleFlowJet10_BTag;   //!
   TBranch        *b_ParticleFlowJet10_BTagAlgo;   //!
   TBranch        *b_ParticleFlowJet10_BTagPhys;   //!
   TBranch        *b_ParticleFlowJet10_TauTag;   //!
   TBranch        *b_ParticleFlowJet10_TauWeight;   //!
   TBranch        *b_ParticleFlowJet10_Charge;   //!
   TBranch        *b_ParticleFlowJet10_EhadOverEem;   //!
   TBranch        *b_ParticleFlowJet10_NCharged;   //!
   TBranch        *b_ParticleFlowJet10_NNeutrals;   //!
   TBranch        *b_ParticleFlowJet10_NeutralEnergyFraction;   //!
   TBranch        *b_ParticleFlowJet10_ChargedEnergyFraction;   //!
   TBranch        *b_ParticleFlowJet10_Beta;   //!
   TBranch        *b_ParticleFlowJet10_BetaStar;   //!
   TBranch        *b_ParticleFlowJet10_MeanSqDeltaR;   //!
   TBranch        *b_ParticleFlowJet10_PTD;   //!
   TBranch        *b_ParticleFlowJet10_FracPt;   //!
   TBranch        *b_ParticleFlowJet10_Tau;   //!
   TBranch        *b_ParticleFlowJet10_SoftDroppedJet;   //!
   TBranch        *b_ParticleFlowJet10_SoftDroppedSubJet1;   //!
   TBranch        *b_ParticleFlowJet10_SoftDroppedSubJet2;   //!
   TBranch        *b_ParticleFlowJet10_TrimmedP4;   //!
   TBranch        *b_ParticleFlowJet10_PrunedP4;   //!
   TBranch        *b_ParticleFlowJet10_SoftDroppedP4;   //!
   TBranch        *b_ParticleFlowJet10_NSubJetsTrimmed;   //!
   TBranch        *b_ParticleFlowJet10_NSubJetsPruned;   //!
   TBranch        *b_ParticleFlowJet10_NSubJetsSoftDropped;   //!
   TBranch        *b_ParticleFlowJet10_ExclYmerge12;   //!
   TBranch        *b_ParticleFlowJet10_ExclYmerge23;   //!
   TBranch        *b_ParticleFlowJet10_ExclYmerge34;   //!
   TBranch        *b_ParticleFlowJet10_ExclYmerge45;   //!
   TBranch        *b_ParticleFlowJet10_ExclYmerge56;   //!
   TBranch        *b_ParticleFlowJet10_Constituents;   //!
   TBranch        *b_ParticleFlowJet10_Particles;   //!
   TBranch        *b_ParticleFlowJet10_Area;   //!
   TBranch        *b_ParticleFlowJet10_size;   //!
   TBranch        *b_GenMissingET_;   //!
   TBranch        *b_GenMissingET_fUniqueID;   //!
   TBranch        *b_GenMissingET_fBits;   //!
   TBranch        *b_GenMissingET_MET;   //!
   TBranch        *b_GenMissingET_Eta;   //!
   TBranch        *b_GenMissingET_Phi;   //!
   TBranch        *b_GenMissingET_size;   //!
   TBranch        *b_Jet_;   //!
   TBranch        *b_Jet_fUniqueID;   //!
   TBranch        *b_Jet_fBits;   //!
   TBranch        *b_Jet_PT;   //!
   TBranch        *b_Jet_Eta;   //!
   TBranch        *b_Jet_Phi;   //!
   TBranch        *b_Jet_T;   //!
   TBranch        *b_Jet_Mass;   //!
   TBranch        *b_Jet_DeltaEta;   //!
   TBranch        *b_Jet_DeltaPhi;   //!
   TBranch        *b_Jet_Flavor;   //!
   TBranch        *b_Jet_FlavorAlgo;   //!
   TBranch        *b_Jet_FlavorPhys;   //!
   TBranch        *b_Jet_TauFlavor;   //!
   TBranch        *b_Jet_BTag;   //!
   TBranch        *b_Jet_BTagAlgo;   //!
   TBranch        *b_Jet_BTagPhys;   //!
   TBranch        *b_Jet_TauTag;   //!
   TBranch        *b_Jet_TauWeight;   //!
   TBranch        *b_Jet_Charge;   //!
   TBranch        *b_Jet_EhadOverEem;   //!
   TBranch        *b_Jet_NCharged;   //!
   TBranch        *b_Jet_NNeutrals;   //!
   TBranch        *b_Jet_NeutralEnergyFraction;   //!
   TBranch        *b_Jet_ChargedEnergyFraction;   //!
   TBranch        *b_Jet_Beta;   //!
   TBranch        *b_Jet_BetaStar;   //!
   TBranch        *b_Jet_MeanSqDeltaR;   //!
   TBranch        *b_Jet_PTD;   //!
   TBranch        *b_Jet_FracPt;   //!
   TBranch        *b_Jet_Tau;   //!
   TBranch        *b_Jet_SoftDroppedJet;   //!
   TBranch        *b_Jet_SoftDroppedSubJet1;   //!
   TBranch        *b_Jet_SoftDroppedSubJet2;   //!
   TBranch        *b_Jet_TrimmedP4;   //!
   TBranch        *b_Jet_PrunedP4;   //!
   TBranch        *b_Jet_SoftDroppedP4;   //!
   TBranch        *b_Jet_NSubJetsTrimmed;   //!
   TBranch        *b_Jet_NSubJetsPruned;   //!
   TBranch        *b_Jet_NSubJetsSoftDropped;   //!
   TBranch        *b_Jet_ExclYmerge12;   //!
   TBranch        *b_Jet_ExclYmerge23;   //!
   TBranch        *b_Jet_ExclYmerge34;   //!
   TBranch        *b_Jet_ExclYmerge45;   //!
   TBranch        *b_Jet_ExclYmerge56;   //!
   TBranch        *b_Jet_Constituents;   //!
   TBranch        *b_Jet_Particles;   //!
   TBranch        *b_Jet_Area;   //!
   TBranch        *b_Jet_size;   //!
   TBranch        *b_Electron_;   //!
   TBranch        *b_Electron_fUniqueID;   //!
   TBranch        *b_Electron_fBits;   //!
   TBranch        *b_Electron_PT;   //!
   TBranch        *b_Electron_Eta;   //!
   TBranch        *b_Electron_Phi;   //!
   TBranch        *b_Electron_T;   //!
   TBranch        *b_Electron_Charge;   //!
   TBranch        *b_Electron_EhadOverEem;   //!
   TBranch        *b_Electron_Particle;   //!
   TBranch        *b_Electron_IsolationVar;   //!
   TBranch        *b_Electron_IsolationVarRhoCorr;   //!
   TBranch        *b_Electron_SumPtCharged;   //!
   TBranch        *b_Electron_SumPtNeutral;   //!
   TBranch        *b_Electron_SumPtChargedPU;   //!
   TBranch        *b_Electron_SumPt;   //!
   TBranch        *b_Electron_D0;   //!
   TBranch        *b_Electron_DZ;   //!
   TBranch        *b_Electron_ErrorD0;   //!
   TBranch        *b_Electron_ErrorDZ;   //!
   TBranch        *b_Electron_size;   //!
   TBranch        *b_Photon_;   //!
   TBranch        *b_Photon_fUniqueID;   //!
   TBranch        *b_Photon_fBits;   //!
   TBranch        *b_Photon_PT;   //!
   TBranch        *b_Photon_Eta;   //!
   TBranch        *b_Photon_Phi;   //!
   TBranch        *b_Photon_E;   //!
   TBranch        *b_Photon_T;   //!
   TBranch        *b_Photon_EhadOverEem;   //!
   TBranch        *b_Photon_Particles;   //!
   TBranch        *b_Photon_IsolationVar;   //!
   TBranch        *b_Photon_IsolationVarRhoCorr;   //!
   TBranch        *b_Photon_SumPtCharged;   //!
   TBranch        *b_Photon_SumPtNeutral;   //!
   TBranch        *b_Photon_SumPtChargedPU;   //!
   TBranch        *b_Photon_SumPt;   //!
   TBranch        *b_Photon_Status;   //!
   TBranch        *b_Photon_size;   //!
   TBranch        *b_Muon_;   //!
   TBranch        *b_Muon_fUniqueID;   //!
   TBranch        *b_Muon_fBits;   //!
   TBranch        *b_Muon_PT;   //!
   TBranch        *b_Muon_Eta;   //!
   TBranch        *b_Muon_Phi;   //!
   TBranch        *b_Muon_T;   //!
   TBranch        *b_Muon_Charge;   //!
   TBranch        *b_Muon_Particle;   //!
   TBranch        *b_Muon_IsolationVar;   //!
   TBranch        *b_Muon_IsolationVarRhoCorr;   //!
   TBranch        *b_Muon_SumPtCharged;   //!
   TBranch        *b_Muon_SumPtNeutral;   //!
   TBranch        *b_Muon_SumPtChargedPU;   //!
   TBranch        *b_Muon_SumPt;   //!
   TBranch        *b_Muon_D0;   //!
   TBranch        *b_Muon_DZ;   //!
   TBranch        *b_Muon_ErrorD0;   //!
   TBranch        *b_Muon_ErrorDZ;   //!
   TBranch        *b_Muon_size;   //!
   TBranch        *b_MissingET_;   //!
   TBranch        *b_MissingET_fUniqueID;   //!
   TBranch        *b_MissingET_fBits;   //!
   TBranch        *b_MissingET_MET;   //!
   TBranch        *b_MissingET_Eta;   //!
   TBranch        *b_MissingET_Phi;   //!
   TBranch        *b_MissingET_size;   //!
   TBranch        *b_ScalarHT_;   //!
   TBranch        *b_ScalarHT_fUniqueID;   //!
   TBranch        *b_ScalarHT_fBits;   //!
   TBranch        *b_ScalarHT_HT;   //!
   TBranch        *b_ScalarHT_size;   //!

   Analysis(TTree *tree=0);
   virtual ~Analysis();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef Analysis_cxx
Analysis::Analysis(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("ntuple_delphes_wqqzll_ATLAS_13TeV_ptll200.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("ntuple_delphes_wqqzll_ATLAS_13TeV_ptll200.root");
      }
      f->GetObject("Delphes",tree);

   }
   Init(tree);
}

Analysis::~Analysis()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t Analysis::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t Analysis::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void Analysis::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("Event", &Event_, &b_Event_);
   fChain->SetBranchAddress("Event.fUniqueID", Event_fUniqueID, &b_Event_fUniqueID);
   fChain->SetBranchAddress("Event.fBits", Event_fBits, &b_Event_fBits);
   fChain->SetBranchAddress("Event.Number", Event_Number, &b_Event_Number);
   fChain->SetBranchAddress("Event.ReadTime", Event_ReadTime, &b_Event_ReadTime);
   fChain->SetBranchAddress("Event.ProcTime", Event_ProcTime, &b_Event_ProcTime);
   fChain->SetBranchAddress("Event.ProcessID", Event_ProcessID, &b_Event_ProcessID);
   fChain->SetBranchAddress("Event.MPI", Event_MPI, &b_Event_MPI);
   fChain->SetBranchAddress("Event.Weight", Event_Weight, &b_Event_Weight);
   fChain->SetBranchAddress("Event.CrossSection", Event_CrossSection, &b_Event_CrossSection);
   fChain->SetBranchAddress("Event.CrossSectionError", Event_CrossSectionError, &b_Event_CrossSectionError);
   fChain->SetBranchAddress("Event.Scale", Event_Scale, &b_Event_Scale);
   fChain->SetBranchAddress("Event.AlphaQED", Event_AlphaQED, &b_Event_AlphaQED);
   fChain->SetBranchAddress("Event.AlphaQCD", Event_AlphaQCD, &b_Event_AlphaQCD);
   fChain->SetBranchAddress("Event.ID1", Event_ID1, &b_Event_ID1);
   fChain->SetBranchAddress("Event.ID2", Event_ID2, &b_Event_ID2);
   fChain->SetBranchAddress("Event.X1", Event_X1, &b_Event_X1);
   fChain->SetBranchAddress("Event.X2", Event_X2, &b_Event_X2);
   fChain->SetBranchAddress("Event.ScalePDF", Event_ScalePDF, &b_Event_ScalePDF);
   fChain->SetBranchAddress("Event.PDF1", Event_PDF1, &b_Event_PDF1);
   fChain->SetBranchAddress("Event.PDF2", Event_PDF2, &b_Event_PDF2);
   fChain->SetBranchAddress("Event_size", &Event_size, &b_Event_size);
   fChain->SetBranchAddress("Weight", &Weight_, &b_Weight_);
   fChain->SetBranchAddress("Weight.fUniqueID", Weight_fUniqueID, &b_Weight_fUniqueID);
   fChain->SetBranchAddress("Weight.fBits", Weight_fBits, &b_Weight_fBits);
   fChain->SetBranchAddress("Weight.Weight", Weight_Weight, &b_Weight_Weight);
   fChain->SetBranchAddress("Weight_size", &Weight_size, &b_Weight_size);
   fChain->SetBranchAddress("Particle", &Particle_, &b_Particle_);
   fChain->SetBranchAddress("Particle.fUniqueID", Particle_fUniqueID, &b_Particle_fUniqueID);
   fChain->SetBranchAddress("Particle.fBits", Particle_fBits, &b_Particle_fBits);
   fChain->SetBranchAddress("Particle.PID", Particle_PID, &b_Particle_PID);
   fChain->SetBranchAddress("Particle.Status", Particle_Status, &b_Particle_Status);
   fChain->SetBranchAddress("Particle.IsPU", Particle_IsPU, &b_Particle_IsPU);
   fChain->SetBranchAddress("Particle.M1", Particle_M1, &b_Particle_M1);
   fChain->SetBranchAddress("Particle.M2", Particle_M2, &b_Particle_M2);
   fChain->SetBranchAddress("Particle.D1", Particle_D1, &b_Particle_D1);
   fChain->SetBranchAddress("Particle.D2", Particle_D2, &b_Particle_D2);
   fChain->SetBranchAddress("Particle.Charge", Particle_Charge, &b_Particle_Charge);
   fChain->SetBranchAddress("Particle.Mass", Particle_Mass, &b_Particle_Mass);
   fChain->SetBranchAddress("Particle.E", Particle_E, &b_Particle_E);
   fChain->SetBranchAddress("Particle.Px", Particle_Px, &b_Particle_Px);
   fChain->SetBranchAddress("Particle.Py", Particle_Py, &b_Particle_Py);
   fChain->SetBranchAddress("Particle.Pz", Particle_Pz, &b_Particle_Pz);
   fChain->SetBranchAddress("Particle.P", Particle_P, &b_Particle_P);
   fChain->SetBranchAddress("Particle.PT", Particle_PT, &b_Particle_PT);
   fChain->SetBranchAddress("Particle.Eta", Particle_Eta, &b_Particle_Eta);
   fChain->SetBranchAddress("Particle.Phi", Particle_Phi, &b_Particle_Phi);
   fChain->SetBranchAddress("Particle.Rapidity", Particle_Rapidity, &b_Particle_Rapidity);
   fChain->SetBranchAddress("Particle.T", Particle_T, &b_Particle_T);
   fChain->SetBranchAddress("Particle.X", Particle_X, &b_Particle_X);
   fChain->SetBranchAddress("Particle.Y", Particle_Y, &b_Particle_Y);
   fChain->SetBranchAddress("Particle.Z", Particle_Z, &b_Particle_Z);
   fChain->SetBranchAddress("Particle_size", &Particle_size, &b_Particle_size);
   fChain->SetBranchAddress("Track", &Track_, &b_Track_);
   fChain->SetBranchAddress("Track.fUniqueID", Track_fUniqueID, &b_Track_fUniqueID);
   fChain->SetBranchAddress("Track.fBits", Track_fBits, &b_Track_fBits);
   fChain->SetBranchAddress("Track.PID", Track_PID, &b_Track_PID);
   fChain->SetBranchAddress("Track.Charge", Track_Charge, &b_Track_Charge);
   fChain->SetBranchAddress("Track.P", Track_P, &b_Track_P);
   fChain->SetBranchAddress("Track.PT", Track_PT, &b_Track_PT);
   fChain->SetBranchAddress("Track.Eta", Track_Eta, &b_Track_Eta);
   fChain->SetBranchAddress("Track.Phi", Track_Phi, &b_Track_Phi);
   fChain->SetBranchAddress("Track.CtgTheta", Track_CtgTheta, &b_Track_CtgTheta);
   fChain->SetBranchAddress("Track.C", Track_C, &b_Track_C);
   fChain->SetBranchAddress("Track.Mass", Track_Mass, &b_Track_Mass);
   fChain->SetBranchAddress("Track.EtaOuter", Track_EtaOuter, &b_Track_EtaOuter);
   fChain->SetBranchAddress("Track.PhiOuter", Track_PhiOuter, &b_Track_PhiOuter);
   fChain->SetBranchAddress("Track.T", Track_T, &b_Track_T);
   fChain->SetBranchAddress("Track.X", Track_X, &b_Track_X);
   fChain->SetBranchAddress("Track.Y", Track_Y, &b_Track_Y);
   fChain->SetBranchAddress("Track.Z", Track_Z, &b_Track_Z);
   fChain->SetBranchAddress("Track.TOuter", Track_TOuter, &b_Track_TOuter);
   fChain->SetBranchAddress("Track.XOuter", Track_XOuter, &b_Track_XOuter);
   fChain->SetBranchAddress("Track.YOuter", Track_YOuter, &b_Track_YOuter);
   fChain->SetBranchAddress("Track.ZOuter", Track_ZOuter, &b_Track_ZOuter);
   fChain->SetBranchAddress("Track.Xd", Track_Xd, &b_Track_Xd);
   fChain->SetBranchAddress("Track.Yd", Track_Yd, &b_Track_Yd);
   fChain->SetBranchAddress("Track.Zd", Track_Zd, &b_Track_Zd);
   fChain->SetBranchAddress("Track.XFirstHit", Track_XFirstHit, &b_Track_XFirstHit);
   fChain->SetBranchAddress("Track.YFirstHit", Track_YFirstHit, &b_Track_YFirstHit);
   fChain->SetBranchAddress("Track.ZFirstHit", Track_ZFirstHit, &b_Track_ZFirstHit);
   fChain->SetBranchAddress("Track.L", Track_L, &b_Track_L);
   fChain->SetBranchAddress("Track.D0", Track_D0, &b_Track_D0);
   fChain->SetBranchAddress("Track.DZ", Track_DZ, &b_Track_DZ);
   fChain->SetBranchAddress("Track.Nclusters", Track_Nclusters, &b_Track_Nclusters);
   fChain->SetBranchAddress("Track.dNdx", Track_dNdx, &b_Track_dNdx);
   fChain->SetBranchAddress("Track.ErrorP", Track_ErrorP, &b_Track_ErrorP);
   fChain->SetBranchAddress("Track.ErrorPT", Track_ErrorPT, &b_Track_ErrorPT);
   fChain->SetBranchAddress("Track.ErrorPhi", Track_ErrorPhi, &b_Track_ErrorPhi);
   fChain->SetBranchAddress("Track.ErrorCtgTheta", Track_ErrorCtgTheta, &b_Track_ErrorCtgTheta);
   fChain->SetBranchAddress("Track.ErrorT", Track_ErrorT, &b_Track_ErrorT);
   fChain->SetBranchAddress("Track.ErrorD0", Track_ErrorD0, &b_Track_ErrorD0);
   fChain->SetBranchAddress("Track.ErrorDZ", Track_ErrorDZ, &b_Track_ErrorDZ);
   fChain->SetBranchAddress("Track.ErrorC", Track_ErrorC, &b_Track_ErrorC);
   fChain->SetBranchAddress("Track.ErrorD0Phi", Track_ErrorD0Phi, &b_Track_ErrorD0Phi);
   fChain->SetBranchAddress("Track.ErrorD0C", Track_ErrorD0C, &b_Track_ErrorD0C);
   fChain->SetBranchAddress("Track.ErrorD0DZ", Track_ErrorD0DZ, &b_Track_ErrorD0DZ);
   fChain->SetBranchAddress("Track.ErrorD0CtgTheta", Track_ErrorD0CtgTheta, &b_Track_ErrorD0CtgTheta);
   fChain->SetBranchAddress("Track.ErrorPhiC", Track_ErrorPhiC, &b_Track_ErrorPhiC);
   fChain->SetBranchAddress("Track.ErrorPhiDZ", Track_ErrorPhiDZ, &b_Track_ErrorPhiDZ);
   fChain->SetBranchAddress("Track.ErrorPhiCtgTheta", Track_ErrorPhiCtgTheta, &b_Track_ErrorPhiCtgTheta);
   fChain->SetBranchAddress("Track.ErrorCDZ", Track_ErrorCDZ, &b_Track_ErrorCDZ);
   fChain->SetBranchAddress("Track.ErrorCCtgTheta", Track_ErrorCCtgTheta, &b_Track_ErrorCCtgTheta);
   fChain->SetBranchAddress("Track.ErrorDZCtgTheta", Track_ErrorDZCtgTheta, &b_Track_ErrorDZCtgTheta);
   fChain->SetBranchAddress("Track.Particle", Track_Particle, &b_Track_Particle);
   fChain->SetBranchAddress("Track.VertexIndex", Track_VertexIndex, &b_Track_VertexIndex);
   fChain->SetBranchAddress("Track_size", &Track_size, &b_Track_size);
   fChain->SetBranchAddress("Tower", &Tower_, &b_Tower_);
   fChain->SetBranchAddress("Tower.fUniqueID", Tower_fUniqueID, &b_Tower_fUniqueID);
   fChain->SetBranchAddress("Tower.fBits", Tower_fBits, &b_Tower_fBits);
   fChain->SetBranchAddress("Tower.ET", Tower_ET, &b_Tower_ET);
   fChain->SetBranchAddress("Tower.Eta", Tower_Eta, &b_Tower_Eta);
   fChain->SetBranchAddress("Tower.Phi", Tower_Phi, &b_Tower_Phi);
   fChain->SetBranchAddress("Tower.E", Tower_E, &b_Tower_E);
   fChain->SetBranchAddress("Tower.T", Tower_T, &b_Tower_T);
   fChain->SetBranchAddress("Tower.X", Tower_X, &b_Tower_X);
   fChain->SetBranchAddress("Tower.Y", Tower_Y, &b_Tower_Y);
   fChain->SetBranchAddress("Tower.Z", Tower_Z, &b_Tower_Z);
   fChain->SetBranchAddress("Tower.NTimeHits", Tower_NTimeHits, &b_Tower_NTimeHits);
   fChain->SetBranchAddress("Tower.Eem", Tower_Eem, &b_Tower_Eem);
   fChain->SetBranchAddress("Tower.Ehad", Tower_Ehad, &b_Tower_Ehad);
   fChain->SetBranchAddress("Tower.Etrk", Tower_Etrk, &b_Tower_Etrk);
   fChain->SetBranchAddress("Tower.Edges[4]", Tower_Edges, &b_Tower_Edges);
   fChain->SetBranchAddress("Tower.Particles", Tower_Particles, &b_Tower_Particles);
   fChain->SetBranchAddress("Tower_size", &Tower_size, &b_Tower_size);
   fChain->SetBranchAddress("EFlowTrack", &EFlowTrack_, &b_EFlowTrack_);
   fChain->SetBranchAddress("EFlowTrack.fUniqueID", EFlowTrack_fUniqueID, &b_EFlowTrack_fUniqueID);
   fChain->SetBranchAddress("EFlowTrack.fBits", EFlowTrack_fBits, &b_EFlowTrack_fBits);
   fChain->SetBranchAddress("EFlowTrack.PID", EFlowTrack_PID, &b_EFlowTrack_PID);
   fChain->SetBranchAddress("EFlowTrack.Charge", EFlowTrack_Charge, &b_EFlowTrack_Charge);
   fChain->SetBranchAddress("EFlowTrack.P", EFlowTrack_P, &b_EFlowTrack_P);
   fChain->SetBranchAddress("EFlowTrack.PT", EFlowTrack_PT, &b_EFlowTrack_PT);
   fChain->SetBranchAddress("EFlowTrack.Eta", EFlowTrack_Eta, &b_EFlowTrack_Eta);
   fChain->SetBranchAddress("EFlowTrack.Phi", EFlowTrack_Phi, &b_EFlowTrack_Phi);
   fChain->SetBranchAddress("EFlowTrack.CtgTheta", EFlowTrack_CtgTheta, &b_EFlowTrack_CtgTheta);
   fChain->SetBranchAddress("EFlowTrack.C", EFlowTrack_C, &b_EFlowTrack_C);
   fChain->SetBranchAddress("EFlowTrack.Mass", EFlowTrack_Mass, &b_EFlowTrack_Mass);
   fChain->SetBranchAddress("EFlowTrack.EtaOuter", EFlowTrack_EtaOuter, &b_EFlowTrack_EtaOuter);
   fChain->SetBranchAddress("EFlowTrack.PhiOuter", EFlowTrack_PhiOuter, &b_EFlowTrack_PhiOuter);
   fChain->SetBranchAddress("EFlowTrack.T", EFlowTrack_T, &b_EFlowTrack_T);
   fChain->SetBranchAddress("EFlowTrack.X", EFlowTrack_X, &b_EFlowTrack_X);
   fChain->SetBranchAddress("EFlowTrack.Y", EFlowTrack_Y, &b_EFlowTrack_Y);
   fChain->SetBranchAddress("EFlowTrack.Z", EFlowTrack_Z, &b_EFlowTrack_Z);
   fChain->SetBranchAddress("EFlowTrack.TOuter", EFlowTrack_TOuter, &b_EFlowTrack_TOuter);
   fChain->SetBranchAddress("EFlowTrack.XOuter", EFlowTrack_XOuter, &b_EFlowTrack_XOuter);
   fChain->SetBranchAddress("EFlowTrack.YOuter", EFlowTrack_YOuter, &b_EFlowTrack_YOuter);
   fChain->SetBranchAddress("EFlowTrack.ZOuter", EFlowTrack_ZOuter, &b_EFlowTrack_ZOuter);
   fChain->SetBranchAddress("EFlowTrack.Xd", EFlowTrack_Xd, &b_EFlowTrack_Xd);
   fChain->SetBranchAddress("EFlowTrack.Yd", EFlowTrack_Yd, &b_EFlowTrack_Yd);
   fChain->SetBranchAddress("EFlowTrack.Zd", EFlowTrack_Zd, &b_EFlowTrack_Zd);
   fChain->SetBranchAddress("EFlowTrack.XFirstHit", EFlowTrack_XFirstHit, &b_EFlowTrack_XFirstHit);
   fChain->SetBranchAddress("EFlowTrack.YFirstHit", EFlowTrack_YFirstHit, &b_EFlowTrack_YFirstHit);
   fChain->SetBranchAddress("EFlowTrack.ZFirstHit", EFlowTrack_ZFirstHit, &b_EFlowTrack_ZFirstHit);
   fChain->SetBranchAddress("EFlowTrack.L", EFlowTrack_L, &b_EFlowTrack_L);
   fChain->SetBranchAddress("EFlowTrack.D0", EFlowTrack_D0, &b_EFlowTrack_D0);
   fChain->SetBranchAddress("EFlowTrack.DZ", EFlowTrack_DZ, &b_EFlowTrack_DZ);
   fChain->SetBranchAddress("EFlowTrack.Nclusters", EFlowTrack_Nclusters, &b_EFlowTrack_Nclusters);
   fChain->SetBranchAddress("EFlowTrack.dNdx", EFlowTrack_dNdx, &b_EFlowTrack_dNdx);
   fChain->SetBranchAddress("EFlowTrack.ErrorP", EFlowTrack_ErrorP, &b_EFlowTrack_ErrorP);
   fChain->SetBranchAddress("EFlowTrack.ErrorPT", EFlowTrack_ErrorPT, &b_EFlowTrack_ErrorPT);
   fChain->SetBranchAddress("EFlowTrack.ErrorPhi", EFlowTrack_ErrorPhi, &b_EFlowTrack_ErrorPhi);
   fChain->SetBranchAddress("EFlowTrack.ErrorCtgTheta", EFlowTrack_ErrorCtgTheta, &b_EFlowTrack_ErrorCtgTheta);
   fChain->SetBranchAddress("EFlowTrack.ErrorT", EFlowTrack_ErrorT, &b_EFlowTrack_ErrorT);
   fChain->SetBranchAddress("EFlowTrack.ErrorD0", EFlowTrack_ErrorD0, &b_EFlowTrack_ErrorD0);
   fChain->SetBranchAddress("EFlowTrack.ErrorDZ", EFlowTrack_ErrorDZ, &b_EFlowTrack_ErrorDZ);
   fChain->SetBranchAddress("EFlowTrack.ErrorC", EFlowTrack_ErrorC, &b_EFlowTrack_ErrorC);
   fChain->SetBranchAddress("EFlowTrack.ErrorD0Phi", EFlowTrack_ErrorD0Phi, &b_EFlowTrack_ErrorD0Phi);
   fChain->SetBranchAddress("EFlowTrack.ErrorD0C", EFlowTrack_ErrorD0C, &b_EFlowTrack_ErrorD0C);
   fChain->SetBranchAddress("EFlowTrack.ErrorD0DZ", EFlowTrack_ErrorD0DZ, &b_EFlowTrack_ErrorD0DZ);
   fChain->SetBranchAddress("EFlowTrack.ErrorD0CtgTheta", EFlowTrack_ErrorD0CtgTheta, &b_EFlowTrack_ErrorD0CtgTheta);
   fChain->SetBranchAddress("EFlowTrack.ErrorPhiC", EFlowTrack_ErrorPhiC, &b_EFlowTrack_ErrorPhiC);
   fChain->SetBranchAddress("EFlowTrack.ErrorPhiDZ", EFlowTrack_ErrorPhiDZ, &b_EFlowTrack_ErrorPhiDZ);
   fChain->SetBranchAddress("EFlowTrack.ErrorPhiCtgTheta", EFlowTrack_ErrorPhiCtgTheta, &b_EFlowTrack_ErrorPhiCtgTheta);
   fChain->SetBranchAddress("EFlowTrack.ErrorCDZ", EFlowTrack_ErrorCDZ, &b_EFlowTrack_ErrorCDZ);
   fChain->SetBranchAddress("EFlowTrack.ErrorCCtgTheta", EFlowTrack_ErrorCCtgTheta, &b_EFlowTrack_ErrorCCtgTheta);
   fChain->SetBranchAddress("EFlowTrack.ErrorDZCtgTheta", EFlowTrack_ErrorDZCtgTheta, &b_EFlowTrack_ErrorDZCtgTheta);
   fChain->SetBranchAddress("EFlowTrack.Particle", EFlowTrack_Particle, &b_EFlowTrack_Particle);
   fChain->SetBranchAddress("EFlowTrack.VertexIndex", EFlowTrack_VertexIndex, &b_EFlowTrack_VertexIndex);
   fChain->SetBranchAddress("EFlowTrack_size", &EFlowTrack_size, &b_EFlowTrack_size);
   fChain->SetBranchAddress("EFlowPhoton", &EFlowPhoton_, &b_EFlowPhoton_);
   fChain->SetBranchAddress("EFlowPhoton.fUniqueID", EFlowPhoton_fUniqueID, &b_EFlowPhoton_fUniqueID);
   fChain->SetBranchAddress("EFlowPhoton.fBits", EFlowPhoton_fBits, &b_EFlowPhoton_fBits);
   fChain->SetBranchAddress("EFlowPhoton.ET", EFlowPhoton_ET, &b_EFlowPhoton_ET);
   fChain->SetBranchAddress("EFlowPhoton.Eta", EFlowPhoton_Eta, &b_EFlowPhoton_Eta);
   fChain->SetBranchAddress("EFlowPhoton.Phi", EFlowPhoton_Phi, &b_EFlowPhoton_Phi);
   fChain->SetBranchAddress("EFlowPhoton.E", EFlowPhoton_E, &b_EFlowPhoton_E);
   fChain->SetBranchAddress("EFlowPhoton.T", EFlowPhoton_T, &b_EFlowPhoton_T);
   fChain->SetBranchAddress("EFlowPhoton.X", EFlowPhoton_X, &b_EFlowPhoton_X);
   fChain->SetBranchAddress("EFlowPhoton.Y", EFlowPhoton_Y, &b_EFlowPhoton_Y);
   fChain->SetBranchAddress("EFlowPhoton.Z", EFlowPhoton_Z, &b_EFlowPhoton_Z);
   fChain->SetBranchAddress("EFlowPhoton.NTimeHits", EFlowPhoton_NTimeHits, &b_EFlowPhoton_NTimeHits);
   fChain->SetBranchAddress("EFlowPhoton.Eem", EFlowPhoton_Eem, &b_EFlowPhoton_Eem);
   fChain->SetBranchAddress("EFlowPhoton.Ehad", EFlowPhoton_Ehad, &b_EFlowPhoton_Ehad);
   fChain->SetBranchAddress("EFlowPhoton.Etrk", EFlowPhoton_Etrk, &b_EFlowPhoton_Etrk);
   fChain->SetBranchAddress("EFlowPhoton.Edges[4]", EFlowPhoton_Edges, &b_EFlowPhoton_Edges);
   fChain->SetBranchAddress("EFlowPhoton.Particles", EFlowPhoton_Particles, &b_EFlowPhoton_Particles);
   fChain->SetBranchAddress("EFlowPhoton_size", &EFlowPhoton_size, &b_EFlowPhoton_size);
   fChain->SetBranchAddress("EFlowNeutralHadron", &EFlowNeutralHadron_, &b_EFlowNeutralHadron_);
   fChain->SetBranchAddress("EFlowNeutralHadron.fUniqueID", EFlowNeutralHadron_fUniqueID, &b_EFlowNeutralHadron_fUniqueID);
   fChain->SetBranchAddress("EFlowNeutralHadron.fBits", EFlowNeutralHadron_fBits, &b_EFlowNeutralHadron_fBits);
   fChain->SetBranchAddress("EFlowNeutralHadron.ET", EFlowNeutralHadron_ET, &b_EFlowNeutralHadron_ET);
   fChain->SetBranchAddress("EFlowNeutralHadron.Eta", EFlowNeutralHadron_Eta, &b_EFlowNeutralHadron_Eta);
   fChain->SetBranchAddress("EFlowNeutralHadron.Phi", EFlowNeutralHadron_Phi, &b_EFlowNeutralHadron_Phi);
   fChain->SetBranchAddress("EFlowNeutralHadron.E", EFlowNeutralHadron_E, &b_EFlowNeutralHadron_E);
   fChain->SetBranchAddress("EFlowNeutralHadron.T", EFlowNeutralHadron_T, &b_EFlowNeutralHadron_T);
   fChain->SetBranchAddress("EFlowNeutralHadron.X", EFlowNeutralHadron_X, &b_EFlowNeutralHadron_X);
   fChain->SetBranchAddress("EFlowNeutralHadron.Y", EFlowNeutralHadron_Y, &b_EFlowNeutralHadron_Y);
   fChain->SetBranchAddress("EFlowNeutralHadron.Z", EFlowNeutralHadron_Z, &b_EFlowNeutralHadron_Z);
   fChain->SetBranchAddress("EFlowNeutralHadron.NTimeHits", EFlowNeutralHadron_NTimeHits, &b_EFlowNeutralHadron_NTimeHits);
   fChain->SetBranchAddress("EFlowNeutralHadron.Eem", EFlowNeutralHadron_Eem, &b_EFlowNeutralHadron_Eem);
   fChain->SetBranchAddress("EFlowNeutralHadron.Ehad", EFlowNeutralHadron_Ehad, &b_EFlowNeutralHadron_Ehad);
   fChain->SetBranchAddress("EFlowNeutralHadron.Etrk", EFlowNeutralHadron_Etrk, &b_EFlowNeutralHadron_Etrk);
   fChain->SetBranchAddress("EFlowNeutralHadron.Edges[4]", EFlowNeutralHadron_Edges, &b_EFlowNeutralHadron_Edges);
   fChain->SetBranchAddress("EFlowNeutralHadron.Particles", EFlowNeutralHadron_Particles, &b_EFlowNeutralHadron_Particles);
   fChain->SetBranchAddress("EFlowNeutralHadron_size", &EFlowNeutralHadron_size, &b_EFlowNeutralHadron_size);
   fChain->SetBranchAddress("GenJet04", &GenJet04_, &b_GenJet04_);
   fChain->SetBranchAddress("GenJet04.fUniqueID", GenJet04_fUniqueID, &b_GenJet04_fUniqueID);
   fChain->SetBranchAddress("GenJet04.fBits", GenJet04_fBits, &b_GenJet04_fBits);
   fChain->SetBranchAddress("GenJet04.PT", GenJet04_PT, &b_GenJet04_PT);
   fChain->SetBranchAddress("GenJet04.Eta", GenJet04_Eta, &b_GenJet04_Eta);
   fChain->SetBranchAddress("GenJet04.Phi", GenJet04_Phi, &b_GenJet04_Phi);
   fChain->SetBranchAddress("GenJet04.T", GenJet04_T, &b_GenJet04_T);
   fChain->SetBranchAddress("GenJet04.Mass", GenJet04_Mass, &b_GenJet04_Mass);
   fChain->SetBranchAddress("GenJet04.DeltaEta", GenJet04_DeltaEta, &b_GenJet04_DeltaEta);
   fChain->SetBranchAddress("GenJet04.DeltaPhi", GenJet04_DeltaPhi, &b_GenJet04_DeltaPhi);
   fChain->SetBranchAddress("GenJet04.Flavor", GenJet04_Flavor, &b_GenJet04_Flavor);
   fChain->SetBranchAddress("GenJet04.FlavorAlgo", GenJet04_FlavorAlgo, &b_GenJet04_FlavorAlgo);
   fChain->SetBranchAddress("GenJet04.FlavorPhys", GenJet04_FlavorPhys, &b_GenJet04_FlavorPhys);
   fChain->SetBranchAddress("GenJet04.TauFlavor", GenJet04_TauFlavor, &b_GenJet04_TauFlavor);
   fChain->SetBranchAddress("GenJet04.BTag", GenJet04_BTag, &b_GenJet04_BTag);
   fChain->SetBranchAddress("GenJet04.BTagAlgo", GenJet04_BTagAlgo, &b_GenJet04_BTagAlgo);
   fChain->SetBranchAddress("GenJet04.BTagPhys", GenJet04_BTagPhys, &b_GenJet04_BTagPhys);
   fChain->SetBranchAddress("GenJet04.TauTag", GenJet04_TauTag, &b_GenJet04_TauTag);
   fChain->SetBranchAddress("GenJet04.TauWeight", GenJet04_TauWeight, &b_GenJet04_TauWeight);
   fChain->SetBranchAddress("GenJet04.Charge", GenJet04_Charge, &b_GenJet04_Charge);
   fChain->SetBranchAddress("GenJet04.EhadOverEem", GenJet04_EhadOverEem, &b_GenJet04_EhadOverEem);
   fChain->SetBranchAddress("GenJet04.NCharged", GenJet04_NCharged, &b_GenJet04_NCharged);
   fChain->SetBranchAddress("GenJet04.NNeutrals", GenJet04_NNeutrals, &b_GenJet04_NNeutrals);
   fChain->SetBranchAddress("GenJet04.NeutralEnergyFraction", GenJet04_NeutralEnergyFraction, &b_GenJet04_NeutralEnergyFraction);
   fChain->SetBranchAddress("GenJet04.ChargedEnergyFraction", GenJet04_ChargedEnergyFraction, &b_GenJet04_ChargedEnergyFraction);
   fChain->SetBranchAddress("GenJet04.Beta", GenJet04_Beta, &b_GenJet04_Beta);
   fChain->SetBranchAddress("GenJet04.BetaStar", GenJet04_BetaStar, &b_GenJet04_BetaStar);
   fChain->SetBranchAddress("GenJet04.MeanSqDeltaR", GenJet04_MeanSqDeltaR, &b_GenJet04_MeanSqDeltaR);
   fChain->SetBranchAddress("GenJet04.PTD", GenJet04_PTD, &b_GenJet04_PTD);
   fChain->SetBranchAddress("GenJet04.FracPt[5]", GenJet04_FracPt, &b_GenJet04_FracPt);
   fChain->SetBranchAddress("GenJet04.Tau[5]", GenJet04_Tau, &b_GenJet04_Tau);
   fChain->SetBranchAddress("GenJet04.SoftDroppedJet", GenJet04_SoftDroppedJet, &b_GenJet04_SoftDroppedJet);
   fChain->SetBranchAddress("GenJet04.SoftDroppedSubJet1", GenJet04_SoftDroppedSubJet1, &b_GenJet04_SoftDroppedSubJet1);
   fChain->SetBranchAddress("GenJet04.SoftDroppedSubJet2", GenJet04_SoftDroppedSubJet2, &b_GenJet04_SoftDroppedSubJet2);
   fChain->SetBranchAddress("GenJet04.TrimmedP4[5]", GenJet04_TrimmedP4, &b_GenJet04_TrimmedP4);
   fChain->SetBranchAddress("GenJet04.PrunedP4[5]", GenJet04_PrunedP4, &b_GenJet04_PrunedP4);
   fChain->SetBranchAddress("GenJet04.SoftDroppedP4[5]", GenJet04_SoftDroppedP4, &b_GenJet04_SoftDroppedP4);
   fChain->SetBranchAddress("GenJet04.NSubJetsTrimmed", GenJet04_NSubJetsTrimmed, &b_GenJet04_NSubJetsTrimmed);
   fChain->SetBranchAddress("GenJet04.NSubJetsPruned", GenJet04_NSubJetsPruned, &b_GenJet04_NSubJetsPruned);
   fChain->SetBranchAddress("GenJet04.NSubJetsSoftDropped", GenJet04_NSubJetsSoftDropped, &b_GenJet04_NSubJetsSoftDropped);
   fChain->SetBranchAddress("GenJet04.ExclYmerge12", GenJet04_ExclYmerge12, &b_GenJet04_ExclYmerge12);
   fChain->SetBranchAddress("GenJet04.ExclYmerge23", GenJet04_ExclYmerge23, &b_GenJet04_ExclYmerge23);
   fChain->SetBranchAddress("GenJet04.ExclYmerge34", GenJet04_ExclYmerge34, &b_GenJet04_ExclYmerge34);
   fChain->SetBranchAddress("GenJet04.ExclYmerge45", GenJet04_ExclYmerge45, &b_GenJet04_ExclYmerge45);
   fChain->SetBranchAddress("GenJet04.ExclYmerge56", GenJet04_ExclYmerge56, &b_GenJet04_ExclYmerge56);
   fChain->SetBranchAddress("GenJet04.Constituents", GenJet04_Constituents, &b_GenJet04_Constituents);
   fChain->SetBranchAddress("GenJet04.Particles", GenJet04_Particles, &b_GenJet04_Particles);
   fChain->SetBranchAddress("GenJet04.Area", GenJet04_Area, &b_GenJet04_Area);
   fChain->SetBranchAddress("GenJet04_size", &GenJet04_size, &b_GenJet04_size);
   fChain->SetBranchAddress("GenJet10", &GenJet10_, &b_GenJet10_);
   fChain->SetBranchAddress("GenJet10.fUniqueID", GenJet10_fUniqueID, &b_GenJet10_fUniqueID);
   fChain->SetBranchAddress("GenJet10.fBits", GenJet10_fBits, &b_GenJet10_fBits);
   fChain->SetBranchAddress("GenJet10.PT", GenJet10_PT, &b_GenJet10_PT);
   fChain->SetBranchAddress("GenJet10.Eta", GenJet10_Eta, &b_GenJet10_Eta);
   fChain->SetBranchAddress("GenJet10.Phi", GenJet10_Phi, &b_GenJet10_Phi);
   fChain->SetBranchAddress("GenJet10.T", GenJet10_T, &b_GenJet10_T);
   fChain->SetBranchAddress("GenJet10.Mass", GenJet10_Mass, &b_GenJet10_Mass);
   fChain->SetBranchAddress("GenJet10.DeltaEta", GenJet10_DeltaEta, &b_GenJet10_DeltaEta);
   fChain->SetBranchAddress("GenJet10.DeltaPhi", GenJet10_DeltaPhi, &b_GenJet10_DeltaPhi);
   fChain->SetBranchAddress("GenJet10.Flavor", GenJet10_Flavor, &b_GenJet10_Flavor);
   fChain->SetBranchAddress("GenJet10.FlavorAlgo", GenJet10_FlavorAlgo, &b_GenJet10_FlavorAlgo);
   fChain->SetBranchAddress("GenJet10.FlavorPhys", GenJet10_FlavorPhys, &b_GenJet10_FlavorPhys);
   fChain->SetBranchAddress("GenJet10.TauFlavor", GenJet10_TauFlavor, &b_GenJet10_TauFlavor);
   fChain->SetBranchAddress("GenJet10.BTag", GenJet10_BTag, &b_GenJet10_BTag);
   fChain->SetBranchAddress("GenJet10.BTagAlgo", GenJet10_BTagAlgo, &b_GenJet10_BTagAlgo);
   fChain->SetBranchAddress("GenJet10.BTagPhys", GenJet10_BTagPhys, &b_GenJet10_BTagPhys);
   fChain->SetBranchAddress("GenJet10.TauTag", GenJet10_TauTag, &b_GenJet10_TauTag);
   fChain->SetBranchAddress("GenJet10.TauWeight", GenJet10_TauWeight, &b_GenJet10_TauWeight);
   fChain->SetBranchAddress("GenJet10.Charge", GenJet10_Charge, &b_GenJet10_Charge);
   fChain->SetBranchAddress("GenJet10.EhadOverEem", GenJet10_EhadOverEem, &b_GenJet10_EhadOverEem);
   fChain->SetBranchAddress("GenJet10.NCharged", GenJet10_NCharged, &b_GenJet10_NCharged);
   fChain->SetBranchAddress("GenJet10.NNeutrals", GenJet10_NNeutrals, &b_GenJet10_NNeutrals);
   fChain->SetBranchAddress("GenJet10.NeutralEnergyFraction", GenJet10_NeutralEnergyFraction, &b_GenJet10_NeutralEnergyFraction);
   fChain->SetBranchAddress("GenJet10.ChargedEnergyFraction", GenJet10_ChargedEnergyFraction, &b_GenJet10_ChargedEnergyFraction);
   fChain->SetBranchAddress("GenJet10.Beta", GenJet10_Beta, &b_GenJet10_Beta);
   fChain->SetBranchAddress("GenJet10.BetaStar", GenJet10_BetaStar, &b_GenJet10_BetaStar);
   fChain->SetBranchAddress("GenJet10.MeanSqDeltaR", GenJet10_MeanSqDeltaR, &b_GenJet10_MeanSqDeltaR);
   fChain->SetBranchAddress("GenJet10.PTD", GenJet10_PTD, &b_GenJet10_PTD);
   fChain->SetBranchAddress("GenJet10.FracPt[5]", GenJet10_FracPt, &b_GenJet10_FracPt);
   fChain->SetBranchAddress("GenJet10.Tau[5]", GenJet10_Tau, &b_GenJet10_Tau);
   fChain->SetBranchAddress("GenJet10.SoftDroppedJet", GenJet10_SoftDroppedJet, &b_GenJet10_SoftDroppedJet);
   fChain->SetBranchAddress("GenJet10.SoftDroppedSubJet1", GenJet10_SoftDroppedSubJet1, &b_GenJet10_SoftDroppedSubJet1);
   fChain->SetBranchAddress("GenJet10.SoftDroppedSubJet2", GenJet10_SoftDroppedSubJet2, &b_GenJet10_SoftDroppedSubJet2);
   fChain->SetBranchAddress("GenJet10.TrimmedP4[5]", GenJet10_TrimmedP4, &b_GenJet10_TrimmedP4);
   fChain->SetBranchAddress("GenJet10.PrunedP4[5]", GenJet10_PrunedP4, &b_GenJet10_PrunedP4);
   fChain->SetBranchAddress("GenJet10.SoftDroppedP4[5]", GenJet10_SoftDroppedP4, &b_GenJet10_SoftDroppedP4);
   fChain->SetBranchAddress("GenJet10.NSubJetsTrimmed", GenJet10_NSubJetsTrimmed, &b_GenJet10_NSubJetsTrimmed);
   fChain->SetBranchAddress("GenJet10.NSubJetsPruned", GenJet10_NSubJetsPruned, &b_GenJet10_NSubJetsPruned);
   fChain->SetBranchAddress("GenJet10.NSubJetsSoftDropped", GenJet10_NSubJetsSoftDropped, &b_GenJet10_NSubJetsSoftDropped);
   fChain->SetBranchAddress("GenJet10.ExclYmerge12", GenJet10_ExclYmerge12, &b_GenJet10_ExclYmerge12);
   fChain->SetBranchAddress("GenJet10.ExclYmerge23", GenJet10_ExclYmerge23, &b_GenJet10_ExclYmerge23);
   fChain->SetBranchAddress("GenJet10.ExclYmerge34", GenJet10_ExclYmerge34, &b_GenJet10_ExclYmerge34);
   fChain->SetBranchAddress("GenJet10.ExclYmerge45", GenJet10_ExclYmerge45, &b_GenJet10_ExclYmerge45);
   fChain->SetBranchAddress("GenJet10.ExclYmerge56", GenJet10_ExclYmerge56, &b_GenJet10_ExclYmerge56);
   fChain->SetBranchAddress("GenJet10.Constituents", GenJet10_Constituents, &b_GenJet10_Constituents);
   fChain->SetBranchAddress("GenJet10.Particles", GenJet10_Particles, &b_GenJet10_Particles);
   fChain->SetBranchAddress("GenJet10.Area", GenJet10_Area, &b_GenJet10_Area);
   fChain->SetBranchAddress("GenJet10_size", &GenJet10_size, &b_GenJet10_size);
   fChain->SetBranchAddress("CaloJet04", &CaloJet04_, &b_CaloJet04_);
   fChain->SetBranchAddress("CaloJet04.fUniqueID", CaloJet04_fUniqueID, &b_CaloJet04_fUniqueID);
   fChain->SetBranchAddress("CaloJet04.fBits", CaloJet04_fBits, &b_CaloJet04_fBits);
   fChain->SetBranchAddress("CaloJet04.PT", CaloJet04_PT, &b_CaloJet04_PT);
   fChain->SetBranchAddress("CaloJet04.Eta", CaloJet04_Eta, &b_CaloJet04_Eta);
   fChain->SetBranchAddress("CaloJet04.Phi", CaloJet04_Phi, &b_CaloJet04_Phi);
   fChain->SetBranchAddress("CaloJet04.T", CaloJet04_T, &b_CaloJet04_T);
   fChain->SetBranchAddress("CaloJet04.Mass", CaloJet04_Mass, &b_CaloJet04_Mass);
   fChain->SetBranchAddress("CaloJet04.DeltaEta", CaloJet04_DeltaEta, &b_CaloJet04_DeltaEta);
   fChain->SetBranchAddress("CaloJet04.DeltaPhi", CaloJet04_DeltaPhi, &b_CaloJet04_DeltaPhi);
   fChain->SetBranchAddress("CaloJet04.Flavor", CaloJet04_Flavor, &b_CaloJet04_Flavor);
   fChain->SetBranchAddress("CaloJet04.FlavorAlgo", CaloJet04_FlavorAlgo, &b_CaloJet04_FlavorAlgo);
   fChain->SetBranchAddress("CaloJet04.FlavorPhys", CaloJet04_FlavorPhys, &b_CaloJet04_FlavorPhys);
   fChain->SetBranchAddress("CaloJet04.TauFlavor", CaloJet04_TauFlavor, &b_CaloJet04_TauFlavor);
   fChain->SetBranchAddress("CaloJet04.BTag", CaloJet04_BTag, &b_CaloJet04_BTag);
   fChain->SetBranchAddress("CaloJet04.BTagAlgo", CaloJet04_BTagAlgo, &b_CaloJet04_BTagAlgo);
   fChain->SetBranchAddress("CaloJet04.BTagPhys", CaloJet04_BTagPhys, &b_CaloJet04_BTagPhys);
   fChain->SetBranchAddress("CaloJet04.TauTag", CaloJet04_TauTag, &b_CaloJet04_TauTag);
   fChain->SetBranchAddress("CaloJet04.TauWeight", CaloJet04_TauWeight, &b_CaloJet04_TauWeight);
   fChain->SetBranchAddress("CaloJet04.Charge", CaloJet04_Charge, &b_CaloJet04_Charge);
   fChain->SetBranchAddress("CaloJet04.EhadOverEem", CaloJet04_EhadOverEem, &b_CaloJet04_EhadOverEem);
   fChain->SetBranchAddress("CaloJet04.NCharged", CaloJet04_NCharged, &b_CaloJet04_NCharged);
   fChain->SetBranchAddress("CaloJet04.NNeutrals", CaloJet04_NNeutrals, &b_CaloJet04_NNeutrals);
   fChain->SetBranchAddress("CaloJet04.NeutralEnergyFraction", CaloJet04_NeutralEnergyFraction, &b_CaloJet04_NeutralEnergyFraction);
   fChain->SetBranchAddress("CaloJet04.ChargedEnergyFraction", CaloJet04_ChargedEnergyFraction, &b_CaloJet04_ChargedEnergyFraction);
   fChain->SetBranchAddress("CaloJet04.Beta", CaloJet04_Beta, &b_CaloJet04_Beta);
   fChain->SetBranchAddress("CaloJet04.BetaStar", CaloJet04_BetaStar, &b_CaloJet04_BetaStar);
   fChain->SetBranchAddress("CaloJet04.MeanSqDeltaR", CaloJet04_MeanSqDeltaR, &b_CaloJet04_MeanSqDeltaR);
   fChain->SetBranchAddress("CaloJet04.PTD", CaloJet04_PTD, &b_CaloJet04_PTD);
   fChain->SetBranchAddress("CaloJet04.FracPt[5]", CaloJet04_FracPt, &b_CaloJet04_FracPt);
   fChain->SetBranchAddress("CaloJet04.Tau[5]", CaloJet04_Tau, &b_CaloJet04_Tau);
   fChain->SetBranchAddress("CaloJet04.SoftDroppedJet", CaloJet04_SoftDroppedJet, &b_CaloJet04_SoftDroppedJet);
   fChain->SetBranchAddress("CaloJet04.SoftDroppedSubJet1", CaloJet04_SoftDroppedSubJet1, &b_CaloJet04_SoftDroppedSubJet1);
   fChain->SetBranchAddress("CaloJet04.SoftDroppedSubJet2", CaloJet04_SoftDroppedSubJet2, &b_CaloJet04_SoftDroppedSubJet2);
   fChain->SetBranchAddress("CaloJet04.TrimmedP4[5]", CaloJet04_TrimmedP4, &b_CaloJet04_TrimmedP4);
   fChain->SetBranchAddress("CaloJet04.PrunedP4[5]", CaloJet04_PrunedP4, &b_CaloJet04_PrunedP4);
   fChain->SetBranchAddress("CaloJet04.SoftDroppedP4[5]", CaloJet04_SoftDroppedP4, &b_CaloJet04_SoftDroppedP4);
   fChain->SetBranchAddress("CaloJet04.NSubJetsTrimmed", CaloJet04_NSubJetsTrimmed, &b_CaloJet04_NSubJetsTrimmed);
   fChain->SetBranchAddress("CaloJet04.NSubJetsPruned", CaloJet04_NSubJetsPruned, &b_CaloJet04_NSubJetsPruned);
   fChain->SetBranchAddress("CaloJet04.NSubJetsSoftDropped", CaloJet04_NSubJetsSoftDropped, &b_CaloJet04_NSubJetsSoftDropped);
   fChain->SetBranchAddress("CaloJet04.ExclYmerge12", CaloJet04_ExclYmerge12, &b_CaloJet04_ExclYmerge12);
   fChain->SetBranchAddress("CaloJet04.ExclYmerge23", CaloJet04_ExclYmerge23, &b_CaloJet04_ExclYmerge23);
   fChain->SetBranchAddress("CaloJet04.ExclYmerge34", CaloJet04_ExclYmerge34, &b_CaloJet04_ExclYmerge34);
   fChain->SetBranchAddress("CaloJet04.ExclYmerge45", CaloJet04_ExclYmerge45, &b_CaloJet04_ExclYmerge45);
   fChain->SetBranchAddress("CaloJet04.ExclYmerge56", CaloJet04_ExclYmerge56, &b_CaloJet04_ExclYmerge56);
   fChain->SetBranchAddress("CaloJet04.Constituents", CaloJet04_Constituents, &b_CaloJet04_Constituents);
   fChain->SetBranchAddress("CaloJet04.Particles", CaloJet04_Particles, &b_CaloJet04_Particles);
   fChain->SetBranchAddress("CaloJet04.Area", CaloJet04_Area, &b_CaloJet04_Area);
   fChain->SetBranchAddress("CaloJet04_size", &CaloJet04_size, &b_CaloJet04_size);
   fChain->SetBranchAddress("CaloJet10", &CaloJet10_, &b_CaloJet10_);
   fChain->SetBranchAddress("CaloJet10.fUniqueID", CaloJet10_fUniqueID, &b_CaloJet10_fUniqueID);
   fChain->SetBranchAddress("CaloJet10.fBits", CaloJet10_fBits, &b_CaloJet10_fBits);
   fChain->SetBranchAddress("CaloJet10.PT", CaloJet10_PT, &b_CaloJet10_PT);
   fChain->SetBranchAddress("CaloJet10.Eta", CaloJet10_Eta, &b_CaloJet10_Eta);
   fChain->SetBranchAddress("CaloJet10.Phi", CaloJet10_Phi, &b_CaloJet10_Phi);
   fChain->SetBranchAddress("CaloJet10.T", CaloJet10_T, &b_CaloJet10_T);
   fChain->SetBranchAddress("CaloJet10.Mass", CaloJet10_Mass, &b_CaloJet10_Mass);
   fChain->SetBranchAddress("CaloJet10.DeltaEta", CaloJet10_DeltaEta, &b_CaloJet10_DeltaEta);
   fChain->SetBranchAddress("CaloJet10.DeltaPhi", CaloJet10_DeltaPhi, &b_CaloJet10_DeltaPhi);
   fChain->SetBranchAddress("CaloJet10.Flavor", CaloJet10_Flavor, &b_CaloJet10_Flavor);
   fChain->SetBranchAddress("CaloJet10.FlavorAlgo", CaloJet10_FlavorAlgo, &b_CaloJet10_FlavorAlgo);
   fChain->SetBranchAddress("CaloJet10.FlavorPhys", CaloJet10_FlavorPhys, &b_CaloJet10_FlavorPhys);
   fChain->SetBranchAddress("CaloJet10.TauFlavor", CaloJet10_TauFlavor, &b_CaloJet10_TauFlavor);
   fChain->SetBranchAddress("CaloJet10.BTag", CaloJet10_BTag, &b_CaloJet10_BTag);
   fChain->SetBranchAddress("CaloJet10.BTagAlgo", CaloJet10_BTagAlgo, &b_CaloJet10_BTagAlgo);
   fChain->SetBranchAddress("CaloJet10.BTagPhys", CaloJet10_BTagPhys, &b_CaloJet10_BTagPhys);
   fChain->SetBranchAddress("CaloJet10.TauTag", CaloJet10_TauTag, &b_CaloJet10_TauTag);
   fChain->SetBranchAddress("CaloJet10.TauWeight", CaloJet10_TauWeight, &b_CaloJet10_TauWeight);
   fChain->SetBranchAddress("CaloJet10.Charge", CaloJet10_Charge, &b_CaloJet10_Charge);
   fChain->SetBranchAddress("CaloJet10.EhadOverEem", CaloJet10_EhadOverEem, &b_CaloJet10_EhadOverEem);
   fChain->SetBranchAddress("CaloJet10.NCharged", CaloJet10_NCharged, &b_CaloJet10_NCharged);
   fChain->SetBranchAddress("CaloJet10.NNeutrals", CaloJet10_NNeutrals, &b_CaloJet10_NNeutrals);
   fChain->SetBranchAddress("CaloJet10.NeutralEnergyFraction", CaloJet10_NeutralEnergyFraction, &b_CaloJet10_NeutralEnergyFraction);
   fChain->SetBranchAddress("CaloJet10.ChargedEnergyFraction", CaloJet10_ChargedEnergyFraction, &b_CaloJet10_ChargedEnergyFraction);
   fChain->SetBranchAddress("CaloJet10.Beta", CaloJet10_Beta, &b_CaloJet10_Beta);
   fChain->SetBranchAddress("CaloJet10.BetaStar", CaloJet10_BetaStar, &b_CaloJet10_BetaStar);
   fChain->SetBranchAddress("CaloJet10.MeanSqDeltaR", CaloJet10_MeanSqDeltaR, &b_CaloJet10_MeanSqDeltaR);
   fChain->SetBranchAddress("CaloJet10.PTD", CaloJet10_PTD, &b_CaloJet10_PTD);
   fChain->SetBranchAddress("CaloJet10.FracPt[5]", CaloJet10_FracPt, &b_CaloJet10_FracPt);
   fChain->SetBranchAddress("CaloJet10.Tau[5]", CaloJet10_Tau, &b_CaloJet10_Tau);
   fChain->SetBranchAddress("CaloJet10.SoftDroppedJet", CaloJet10_SoftDroppedJet, &b_CaloJet10_SoftDroppedJet);
   fChain->SetBranchAddress("CaloJet10.SoftDroppedSubJet1", CaloJet10_SoftDroppedSubJet1, &b_CaloJet10_SoftDroppedSubJet1);
   fChain->SetBranchAddress("CaloJet10.SoftDroppedSubJet2", CaloJet10_SoftDroppedSubJet2, &b_CaloJet10_SoftDroppedSubJet2);
   fChain->SetBranchAddress("CaloJet10.TrimmedP4[5]", CaloJet10_TrimmedP4, &b_CaloJet10_TrimmedP4);
   fChain->SetBranchAddress("CaloJet10.PrunedP4[5]", CaloJet10_PrunedP4, &b_CaloJet10_PrunedP4);
   fChain->SetBranchAddress("CaloJet10.SoftDroppedP4[5]", CaloJet10_SoftDroppedP4, &b_CaloJet10_SoftDroppedP4);
   fChain->SetBranchAddress("CaloJet10.NSubJetsTrimmed", CaloJet10_NSubJetsTrimmed, &b_CaloJet10_NSubJetsTrimmed);
   fChain->SetBranchAddress("CaloJet10.NSubJetsPruned", CaloJet10_NSubJetsPruned, &b_CaloJet10_NSubJetsPruned);
   fChain->SetBranchAddress("CaloJet10.NSubJetsSoftDropped", CaloJet10_NSubJetsSoftDropped, &b_CaloJet10_NSubJetsSoftDropped);
   fChain->SetBranchAddress("CaloJet10.ExclYmerge12", CaloJet10_ExclYmerge12, &b_CaloJet10_ExclYmerge12);
   fChain->SetBranchAddress("CaloJet10.ExclYmerge23", CaloJet10_ExclYmerge23, &b_CaloJet10_ExclYmerge23);
   fChain->SetBranchAddress("CaloJet10.ExclYmerge34", CaloJet10_ExclYmerge34, &b_CaloJet10_ExclYmerge34);
   fChain->SetBranchAddress("CaloJet10.ExclYmerge45", CaloJet10_ExclYmerge45, &b_CaloJet10_ExclYmerge45);
   fChain->SetBranchAddress("CaloJet10.ExclYmerge56", CaloJet10_ExclYmerge56, &b_CaloJet10_ExclYmerge56);
   fChain->SetBranchAddress("CaloJet10.Constituents", CaloJet10_Constituents, &b_CaloJet10_Constituents);
   fChain->SetBranchAddress("CaloJet10.Particles", CaloJet10_Particles, &b_CaloJet10_Particles);
   fChain->SetBranchAddress("CaloJet10.Area", CaloJet10_Area, &b_CaloJet10_Area);
   fChain->SetBranchAddress("CaloJet10_size", &CaloJet10_size, &b_CaloJet10_size);
   fChain->SetBranchAddress("ParticleFlowJet04", &ParticleFlowJet04_, &b_ParticleFlowJet04_);
   fChain->SetBranchAddress("ParticleFlowJet04.fUniqueID", ParticleFlowJet04_fUniqueID, &b_ParticleFlowJet04_fUniqueID);
   fChain->SetBranchAddress("ParticleFlowJet04.fBits", ParticleFlowJet04_fBits, &b_ParticleFlowJet04_fBits);
   fChain->SetBranchAddress("ParticleFlowJet04.PT", ParticleFlowJet04_PT, &b_ParticleFlowJet04_PT);
   fChain->SetBranchAddress("ParticleFlowJet04.Eta", ParticleFlowJet04_Eta, &b_ParticleFlowJet04_Eta);
   fChain->SetBranchAddress("ParticleFlowJet04.Phi", ParticleFlowJet04_Phi, &b_ParticleFlowJet04_Phi);
   fChain->SetBranchAddress("ParticleFlowJet04.T", ParticleFlowJet04_T, &b_ParticleFlowJet04_T);
   fChain->SetBranchAddress("ParticleFlowJet04.Mass", ParticleFlowJet04_Mass, &b_ParticleFlowJet04_Mass);
   fChain->SetBranchAddress("ParticleFlowJet04.DeltaEta", ParticleFlowJet04_DeltaEta, &b_ParticleFlowJet04_DeltaEta);
   fChain->SetBranchAddress("ParticleFlowJet04.DeltaPhi", ParticleFlowJet04_DeltaPhi, &b_ParticleFlowJet04_DeltaPhi);
   fChain->SetBranchAddress("ParticleFlowJet04.Flavor", ParticleFlowJet04_Flavor, &b_ParticleFlowJet04_Flavor);
   fChain->SetBranchAddress("ParticleFlowJet04.FlavorAlgo", ParticleFlowJet04_FlavorAlgo, &b_ParticleFlowJet04_FlavorAlgo);
   fChain->SetBranchAddress("ParticleFlowJet04.FlavorPhys", ParticleFlowJet04_FlavorPhys, &b_ParticleFlowJet04_FlavorPhys);
   fChain->SetBranchAddress("ParticleFlowJet04.TauFlavor", ParticleFlowJet04_TauFlavor, &b_ParticleFlowJet04_TauFlavor);
   fChain->SetBranchAddress("ParticleFlowJet04.BTag", ParticleFlowJet04_BTag, &b_ParticleFlowJet04_BTag);
   fChain->SetBranchAddress("ParticleFlowJet04.BTagAlgo", ParticleFlowJet04_BTagAlgo, &b_ParticleFlowJet04_BTagAlgo);
   fChain->SetBranchAddress("ParticleFlowJet04.BTagPhys", ParticleFlowJet04_BTagPhys, &b_ParticleFlowJet04_BTagPhys);
   fChain->SetBranchAddress("ParticleFlowJet04.TauTag", ParticleFlowJet04_TauTag, &b_ParticleFlowJet04_TauTag);
   fChain->SetBranchAddress("ParticleFlowJet04.TauWeight", ParticleFlowJet04_TauWeight, &b_ParticleFlowJet04_TauWeight);
   fChain->SetBranchAddress("ParticleFlowJet04.Charge", ParticleFlowJet04_Charge, &b_ParticleFlowJet04_Charge);
   fChain->SetBranchAddress("ParticleFlowJet04.EhadOverEem", ParticleFlowJet04_EhadOverEem, &b_ParticleFlowJet04_EhadOverEem);
   fChain->SetBranchAddress("ParticleFlowJet04.NCharged", ParticleFlowJet04_NCharged, &b_ParticleFlowJet04_NCharged);
   fChain->SetBranchAddress("ParticleFlowJet04.NNeutrals", ParticleFlowJet04_NNeutrals, &b_ParticleFlowJet04_NNeutrals);
   fChain->SetBranchAddress("ParticleFlowJet04.NeutralEnergyFraction", ParticleFlowJet04_NeutralEnergyFraction, &b_ParticleFlowJet04_NeutralEnergyFraction);
   fChain->SetBranchAddress("ParticleFlowJet04.ChargedEnergyFraction", ParticleFlowJet04_ChargedEnergyFraction, &b_ParticleFlowJet04_ChargedEnergyFraction);
   fChain->SetBranchAddress("ParticleFlowJet04.Beta", ParticleFlowJet04_Beta, &b_ParticleFlowJet04_Beta);
   fChain->SetBranchAddress("ParticleFlowJet04.BetaStar", ParticleFlowJet04_BetaStar, &b_ParticleFlowJet04_BetaStar);
   fChain->SetBranchAddress("ParticleFlowJet04.MeanSqDeltaR", ParticleFlowJet04_MeanSqDeltaR, &b_ParticleFlowJet04_MeanSqDeltaR);
   fChain->SetBranchAddress("ParticleFlowJet04.PTD", ParticleFlowJet04_PTD, &b_ParticleFlowJet04_PTD);
   fChain->SetBranchAddress("ParticleFlowJet04.FracPt[5]", ParticleFlowJet04_FracPt, &b_ParticleFlowJet04_FracPt);
   fChain->SetBranchAddress("ParticleFlowJet04.Tau[5]", ParticleFlowJet04_Tau, &b_ParticleFlowJet04_Tau);
   fChain->SetBranchAddress("ParticleFlowJet04.SoftDroppedJet", ParticleFlowJet04_SoftDroppedJet, &b_ParticleFlowJet04_SoftDroppedJet);
   fChain->SetBranchAddress("ParticleFlowJet04.SoftDroppedSubJet1", ParticleFlowJet04_SoftDroppedSubJet1, &b_ParticleFlowJet04_SoftDroppedSubJet1);
   fChain->SetBranchAddress("ParticleFlowJet04.SoftDroppedSubJet2", ParticleFlowJet04_SoftDroppedSubJet2, &b_ParticleFlowJet04_SoftDroppedSubJet2);
   fChain->SetBranchAddress("ParticleFlowJet04.TrimmedP4[5]", ParticleFlowJet04_TrimmedP4, &b_ParticleFlowJet04_TrimmedP4);
   fChain->SetBranchAddress("ParticleFlowJet04.PrunedP4[5]", ParticleFlowJet04_PrunedP4, &b_ParticleFlowJet04_PrunedP4);
   fChain->SetBranchAddress("ParticleFlowJet04.SoftDroppedP4[5]", ParticleFlowJet04_SoftDroppedP4, &b_ParticleFlowJet04_SoftDroppedP4);
   fChain->SetBranchAddress("ParticleFlowJet04.NSubJetsTrimmed", ParticleFlowJet04_NSubJetsTrimmed, &b_ParticleFlowJet04_NSubJetsTrimmed);
   fChain->SetBranchAddress("ParticleFlowJet04.NSubJetsPruned", ParticleFlowJet04_NSubJetsPruned, &b_ParticleFlowJet04_NSubJetsPruned);
   fChain->SetBranchAddress("ParticleFlowJet04.NSubJetsSoftDropped", ParticleFlowJet04_NSubJetsSoftDropped, &b_ParticleFlowJet04_NSubJetsSoftDropped);
   fChain->SetBranchAddress("ParticleFlowJet04.ExclYmerge12", ParticleFlowJet04_ExclYmerge12, &b_ParticleFlowJet04_ExclYmerge12);
   fChain->SetBranchAddress("ParticleFlowJet04.ExclYmerge23", ParticleFlowJet04_ExclYmerge23, &b_ParticleFlowJet04_ExclYmerge23);
   fChain->SetBranchAddress("ParticleFlowJet04.ExclYmerge34", ParticleFlowJet04_ExclYmerge34, &b_ParticleFlowJet04_ExclYmerge34);
   fChain->SetBranchAddress("ParticleFlowJet04.ExclYmerge45", ParticleFlowJet04_ExclYmerge45, &b_ParticleFlowJet04_ExclYmerge45);
   fChain->SetBranchAddress("ParticleFlowJet04.ExclYmerge56", ParticleFlowJet04_ExclYmerge56, &b_ParticleFlowJet04_ExclYmerge56);
   fChain->SetBranchAddress("ParticleFlowJet04.Constituents", ParticleFlowJet04_Constituents, &b_ParticleFlowJet04_Constituents);
   fChain->SetBranchAddress("ParticleFlowJet04.Particles", ParticleFlowJet04_Particles, &b_ParticleFlowJet04_Particles);
   fChain->SetBranchAddress("ParticleFlowJet04.Area", ParticleFlowJet04_Area, &b_ParticleFlowJet04_Area);
   fChain->SetBranchAddress("ParticleFlowJet04_size", &ParticleFlowJet04_size, &b_ParticleFlowJet04_size);
   fChain->SetBranchAddress("ParticleFlowJet10", &ParticleFlowJet10_, &b_ParticleFlowJet10_);
   fChain->SetBranchAddress("ParticleFlowJet10.fUniqueID", ParticleFlowJet10_fUniqueID, &b_ParticleFlowJet10_fUniqueID);
   fChain->SetBranchAddress("ParticleFlowJet10.fBits", ParticleFlowJet10_fBits, &b_ParticleFlowJet10_fBits);
   fChain->SetBranchAddress("ParticleFlowJet10.PT", ParticleFlowJet10_PT, &b_ParticleFlowJet10_PT);
   fChain->SetBranchAddress("ParticleFlowJet10.Eta", ParticleFlowJet10_Eta, &b_ParticleFlowJet10_Eta);
   fChain->SetBranchAddress("ParticleFlowJet10.Phi", ParticleFlowJet10_Phi, &b_ParticleFlowJet10_Phi);
   fChain->SetBranchAddress("ParticleFlowJet10.T", ParticleFlowJet10_T, &b_ParticleFlowJet10_T);
   fChain->SetBranchAddress("ParticleFlowJet10.Mass", ParticleFlowJet10_Mass, &b_ParticleFlowJet10_Mass);
   fChain->SetBranchAddress("ParticleFlowJet10.DeltaEta", ParticleFlowJet10_DeltaEta, &b_ParticleFlowJet10_DeltaEta);
   fChain->SetBranchAddress("ParticleFlowJet10.DeltaPhi", ParticleFlowJet10_DeltaPhi, &b_ParticleFlowJet10_DeltaPhi);
   fChain->SetBranchAddress("ParticleFlowJet10.Flavor", ParticleFlowJet10_Flavor, &b_ParticleFlowJet10_Flavor);
   fChain->SetBranchAddress("ParticleFlowJet10.FlavorAlgo", ParticleFlowJet10_FlavorAlgo, &b_ParticleFlowJet10_FlavorAlgo);
   fChain->SetBranchAddress("ParticleFlowJet10.FlavorPhys", ParticleFlowJet10_FlavorPhys, &b_ParticleFlowJet10_FlavorPhys);
   fChain->SetBranchAddress("ParticleFlowJet10.TauFlavor", ParticleFlowJet10_TauFlavor, &b_ParticleFlowJet10_TauFlavor);
   fChain->SetBranchAddress("ParticleFlowJet10.BTag", ParticleFlowJet10_BTag, &b_ParticleFlowJet10_BTag);
   fChain->SetBranchAddress("ParticleFlowJet10.BTagAlgo", ParticleFlowJet10_BTagAlgo, &b_ParticleFlowJet10_BTagAlgo);
   fChain->SetBranchAddress("ParticleFlowJet10.BTagPhys", ParticleFlowJet10_BTagPhys, &b_ParticleFlowJet10_BTagPhys);
   fChain->SetBranchAddress("ParticleFlowJet10.TauTag", ParticleFlowJet10_TauTag, &b_ParticleFlowJet10_TauTag);
   fChain->SetBranchAddress("ParticleFlowJet10.TauWeight", ParticleFlowJet10_TauWeight, &b_ParticleFlowJet10_TauWeight);
   fChain->SetBranchAddress("ParticleFlowJet10.Charge", ParticleFlowJet10_Charge, &b_ParticleFlowJet10_Charge);
   fChain->SetBranchAddress("ParticleFlowJet10.EhadOverEem", ParticleFlowJet10_EhadOverEem, &b_ParticleFlowJet10_EhadOverEem);
   fChain->SetBranchAddress("ParticleFlowJet10.NCharged", ParticleFlowJet10_NCharged, &b_ParticleFlowJet10_NCharged);
   fChain->SetBranchAddress("ParticleFlowJet10.NNeutrals", ParticleFlowJet10_NNeutrals, &b_ParticleFlowJet10_NNeutrals);
   fChain->SetBranchAddress("ParticleFlowJet10.NeutralEnergyFraction", ParticleFlowJet10_NeutralEnergyFraction, &b_ParticleFlowJet10_NeutralEnergyFraction);
   fChain->SetBranchAddress("ParticleFlowJet10.ChargedEnergyFraction", ParticleFlowJet10_ChargedEnergyFraction, &b_ParticleFlowJet10_ChargedEnergyFraction);
   fChain->SetBranchAddress("ParticleFlowJet10.Beta", ParticleFlowJet10_Beta, &b_ParticleFlowJet10_Beta);
   fChain->SetBranchAddress("ParticleFlowJet10.BetaStar", ParticleFlowJet10_BetaStar, &b_ParticleFlowJet10_BetaStar);
   fChain->SetBranchAddress("ParticleFlowJet10.MeanSqDeltaR", ParticleFlowJet10_MeanSqDeltaR, &b_ParticleFlowJet10_MeanSqDeltaR);
   fChain->SetBranchAddress("ParticleFlowJet10.PTD", ParticleFlowJet10_PTD, &b_ParticleFlowJet10_PTD);
   fChain->SetBranchAddress("ParticleFlowJet10.FracPt[5]", ParticleFlowJet10_FracPt, &b_ParticleFlowJet10_FracPt);
   fChain->SetBranchAddress("ParticleFlowJet10.Tau[5]", ParticleFlowJet10_Tau, &b_ParticleFlowJet10_Tau);
   fChain->SetBranchAddress("ParticleFlowJet10.SoftDroppedJet", ParticleFlowJet10_SoftDroppedJet, &b_ParticleFlowJet10_SoftDroppedJet);
   fChain->SetBranchAddress("ParticleFlowJet10.SoftDroppedSubJet1", ParticleFlowJet10_SoftDroppedSubJet1, &b_ParticleFlowJet10_SoftDroppedSubJet1);
   fChain->SetBranchAddress("ParticleFlowJet10.SoftDroppedSubJet2", ParticleFlowJet10_SoftDroppedSubJet2, &b_ParticleFlowJet10_SoftDroppedSubJet2);
   fChain->SetBranchAddress("ParticleFlowJet10.TrimmedP4[5]", ParticleFlowJet10_TrimmedP4, &b_ParticleFlowJet10_TrimmedP4);
   fChain->SetBranchAddress("ParticleFlowJet10.PrunedP4[5]", ParticleFlowJet10_PrunedP4, &b_ParticleFlowJet10_PrunedP4);
   fChain->SetBranchAddress("ParticleFlowJet10.SoftDroppedP4[5]", ParticleFlowJet10_SoftDroppedP4, &b_ParticleFlowJet10_SoftDroppedP4);
   fChain->SetBranchAddress("ParticleFlowJet10.NSubJetsTrimmed", ParticleFlowJet10_NSubJetsTrimmed, &b_ParticleFlowJet10_NSubJetsTrimmed);
   fChain->SetBranchAddress("ParticleFlowJet10.NSubJetsPruned", ParticleFlowJet10_NSubJetsPruned, &b_ParticleFlowJet10_NSubJetsPruned);
   fChain->SetBranchAddress("ParticleFlowJet10.NSubJetsSoftDropped", ParticleFlowJet10_NSubJetsSoftDropped, &b_ParticleFlowJet10_NSubJetsSoftDropped);
   fChain->SetBranchAddress("ParticleFlowJet10.ExclYmerge12", ParticleFlowJet10_ExclYmerge12, &b_ParticleFlowJet10_ExclYmerge12);
   fChain->SetBranchAddress("ParticleFlowJet10.ExclYmerge23", ParticleFlowJet10_ExclYmerge23, &b_ParticleFlowJet10_ExclYmerge23);
   fChain->SetBranchAddress("ParticleFlowJet10.ExclYmerge34", ParticleFlowJet10_ExclYmerge34, &b_ParticleFlowJet10_ExclYmerge34);
   fChain->SetBranchAddress("ParticleFlowJet10.ExclYmerge45", ParticleFlowJet10_ExclYmerge45, &b_ParticleFlowJet10_ExclYmerge45);
   fChain->SetBranchAddress("ParticleFlowJet10.ExclYmerge56", ParticleFlowJet10_ExclYmerge56, &b_ParticleFlowJet10_ExclYmerge56);
   fChain->SetBranchAddress("ParticleFlowJet10.Constituents", ParticleFlowJet10_Constituents, &b_ParticleFlowJet10_Constituents);
   fChain->SetBranchAddress("ParticleFlowJet10.Particles", ParticleFlowJet10_Particles, &b_ParticleFlowJet10_Particles);
   fChain->SetBranchAddress("ParticleFlowJet10.Area", ParticleFlowJet10_Area, &b_ParticleFlowJet10_Area);
   fChain->SetBranchAddress("ParticleFlowJet10_size", &ParticleFlowJet10_size, &b_ParticleFlowJet10_size);
   fChain->SetBranchAddress("GenMissingET", &GenMissingET_, &b_GenMissingET_);
   fChain->SetBranchAddress("GenMissingET.fUniqueID", GenMissingET_fUniqueID, &b_GenMissingET_fUniqueID);
   fChain->SetBranchAddress("GenMissingET.fBits", GenMissingET_fBits, &b_GenMissingET_fBits);
   fChain->SetBranchAddress("GenMissingET.MET", GenMissingET_MET, &b_GenMissingET_MET);
   fChain->SetBranchAddress("GenMissingET.Eta", GenMissingET_Eta, &b_GenMissingET_Eta);
   fChain->SetBranchAddress("GenMissingET.Phi", GenMissingET_Phi, &b_GenMissingET_Phi);
   fChain->SetBranchAddress("GenMissingET_size", &GenMissingET_size, &b_GenMissingET_size);
   fChain->SetBranchAddress("Jet", &Jet_, &b_Jet_);
   fChain->SetBranchAddress("Jet.fUniqueID", Jet_fUniqueID, &b_Jet_fUniqueID);
   fChain->SetBranchAddress("Jet.fBits", Jet_fBits, &b_Jet_fBits);
   fChain->SetBranchAddress("Jet.PT", Jet_PT, &b_Jet_PT);
   fChain->SetBranchAddress("Jet.Eta", Jet_Eta, &b_Jet_Eta);
   fChain->SetBranchAddress("Jet.Phi", Jet_Phi, &b_Jet_Phi);
   fChain->SetBranchAddress("Jet.T", Jet_T, &b_Jet_T);
   fChain->SetBranchAddress("Jet.Mass", Jet_Mass, &b_Jet_Mass);
   fChain->SetBranchAddress("Jet.DeltaEta", Jet_DeltaEta, &b_Jet_DeltaEta);
   fChain->SetBranchAddress("Jet.DeltaPhi", Jet_DeltaPhi, &b_Jet_DeltaPhi);
   fChain->SetBranchAddress("Jet.Flavor", Jet_Flavor, &b_Jet_Flavor);
   fChain->SetBranchAddress("Jet.FlavorAlgo", Jet_FlavorAlgo, &b_Jet_FlavorAlgo);
   fChain->SetBranchAddress("Jet.FlavorPhys", Jet_FlavorPhys, &b_Jet_FlavorPhys);
   fChain->SetBranchAddress("Jet.TauFlavor", Jet_TauFlavor, &b_Jet_TauFlavor);
   fChain->SetBranchAddress("Jet.BTag", Jet_BTag, &b_Jet_BTag);
   fChain->SetBranchAddress("Jet.BTagAlgo", Jet_BTagAlgo, &b_Jet_BTagAlgo);
   fChain->SetBranchAddress("Jet.BTagPhys", Jet_BTagPhys, &b_Jet_BTagPhys);
   fChain->SetBranchAddress("Jet.TauTag", Jet_TauTag, &b_Jet_TauTag);
   fChain->SetBranchAddress("Jet.TauWeight", Jet_TauWeight, &b_Jet_TauWeight);
   fChain->SetBranchAddress("Jet.Charge", Jet_Charge, &b_Jet_Charge);
   fChain->SetBranchAddress("Jet.EhadOverEem", Jet_EhadOverEem, &b_Jet_EhadOverEem);
   fChain->SetBranchAddress("Jet.NCharged", Jet_NCharged, &b_Jet_NCharged);
   fChain->SetBranchAddress("Jet.NNeutrals", Jet_NNeutrals, &b_Jet_NNeutrals);
   fChain->SetBranchAddress("Jet.NeutralEnergyFraction", Jet_NeutralEnergyFraction, &b_Jet_NeutralEnergyFraction);
   fChain->SetBranchAddress("Jet.ChargedEnergyFraction", Jet_ChargedEnergyFraction, &b_Jet_ChargedEnergyFraction);
   fChain->SetBranchAddress("Jet.Beta", Jet_Beta, &b_Jet_Beta);
   fChain->SetBranchAddress("Jet.BetaStar", Jet_BetaStar, &b_Jet_BetaStar);
   fChain->SetBranchAddress("Jet.MeanSqDeltaR", Jet_MeanSqDeltaR, &b_Jet_MeanSqDeltaR);
   fChain->SetBranchAddress("Jet.PTD", Jet_PTD, &b_Jet_PTD);
   fChain->SetBranchAddress("Jet.FracPt[5]", Jet_FracPt, &b_Jet_FracPt);
   fChain->SetBranchAddress("Jet.Tau[5]", Jet_Tau, &b_Jet_Tau);
   fChain->SetBranchAddress("Jet.SoftDroppedJet", Jet_SoftDroppedJet, &b_Jet_SoftDroppedJet);
   fChain->SetBranchAddress("Jet.SoftDroppedSubJet1", Jet_SoftDroppedSubJet1, &b_Jet_SoftDroppedSubJet1);
   fChain->SetBranchAddress("Jet.SoftDroppedSubJet2", Jet_SoftDroppedSubJet2, &b_Jet_SoftDroppedSubJet2);
   fChain->SetBranchAddress("Jet.TrimmedP4[5]", Jet_TrimmedP4, &b_Jet_TrimmedP4);
   fChain->SetBranchAddress("Jet.PrunedP4[5]", Jet_PrunedP4, &b_Jet_PrunedP4);
   fChain->SetBranchAddress("Jet.SoftDroppedP4[5]", Jet_SoftDroppedP4, &b_Jet_SoftDroppedP4);
   fChain->SetBranchAddress("Jet.NSubJetsTrimmed", Jet_NSubJetsTrimmed, &b_Jet_NSubJetsTrimmed);
   fChain->SetBranchAddress("Jet.NSubJetsPruned", Jet_NSubJetsPruned, &b_Jet_NSubJetsPruned);
   fChain->SetBranchAddress("Jet.NSubJetsSoftDropped", Jet_NSubJetsSoftDropped, &b_Jet_NSubJetsSoftDropped);
   fChain->SetBranchAddress("Jet.ExclYmerge12", Jet_ExclYmerge12, &b_Jet_ExclYmerge12);
   fChain->SetBranchAddress("Jet.ExclYmerge23", Jet_ExclYmerge23, &b_Jet_ExclYmerge23);
   fChain->SetBranchAddress("Jet.ExclYmerge34", Jet_ExclYmerge34, &b_Jet_ExclYmerge34);
   fChain->SetBranchAddress("Jet.ExclYmerge45", Jet_ExclYmerge45, &b_Jet_ExclYmerge45);
   fChain->SetBranchAddress("Jet.ExclYmerge56", Jet_ExclYmerge56, &b_Jet_ExclYmerge56);
   fChain->SetBranchAddress("Jet.Constituents", Jet_Constituents, &b_Jet_Constituents);
   fChain->SetBranchAddress("Jet.Particles", Jet_Particles, &b_Jet_Particles);
   fChain->SetBranchAddress("Jet.Area", Jet_Area, &b_Jet_Area);
   fChain->SetBranchAddress("Jet_size", &Jet_size, &b_Jet_size);
   fChain->SetBranchAddress("Electron", &Electron_, &b_Electron_);
   fChain->SetBranchAddress("Electron.fUniqueID", Electron_fUniqueID, &b_Electron_fUniqueID);
   fChain->SetBranchAddress("Electron.fBits", Electron_fBits, &b_Electron_fBits);
   fChain->SetBranchAddress("Electron.PT", Electron_PT, &b_Electron_PT);
   fChain->SetBranchAddress("Electron.Eta", Electron_Eta, &b_Electron_Eta);
   fChain->SetBranchAddress("Electron.Phi", Electron_Phi, &b_Electron_Phi);
   fChain->SetBranchAddress("Electron.T", Electron_T, &b_Electron_T);
   fChain->SetBranchAddress("Electron.Charge", Electron_Charge, &b_Electron_Charge);
   fChain->SetBranchAddress("Electron.EhadOverEem", Electron_EhadOverEem, &b_Electron_EhadOverEem);
   fChain->SetBranchAddress("Electron.Particle", Electron_Particle, &b_Electron_Particle);
   fChain->SetBranchAddress("Electron.IsolationVar", Electron_IsolationVar, &b_Electron_IsolationVar);
   fChain->SetBranchAddress("Electron.IsolationVarRhoCorr", Electron_IsolationVarRhoCorr, &b_Electron_IsolationVarRhoCorr);
   fChain->SetBranchAddress("Electron.SumPtCharged", Electron_SumPtCharged, &b_Electron_SumPtCharged);
   fChain->SetBranchAddress("Electron.SumPtNeutral", Electron_SumPtNeutral, &b_Electron_SumPtNeutral);
   fChain->SetBranchAddress("Electron.SumPtChargedPU", Electron_SumPtChargedPU, &b_Electron_SumPtChargedPU);
   fChain->SetBranchAddress("Electron.SumPt", Electron_SumPt, &b_Electron_SumPt);
   fChain->SetBranchAddress("Electron.D0", Electron_D0, &b_Electron_D0);
   fChain->SetBranchAddress("Electron.DZ", Electron_DZ, &b_Electron_DZ);
   fChain->SetBranchAddress("Electron.ErrorD0", Electron_ErrorD0, &b_Electron_ErrorD0);
   fChain->SetBranchAddress("Electron.ErrorDZ", Electron_ErrorDZ, &b_Electron_ErrorDZ);
   fChain->SetBranchAddress("Electron_size", &Electron_size, &b_Electron_size);
   fChain->SetBranchAddress("Photon", &Photon_, &b_Photon_);
   fChain->SetBranchAddress("Photon.fUniqueID", Photon_fUniqueID, &b_Photon_fUniqueID);
   fChain->SetBranchAddress("Photon.fBits", Photon_fBits, &b_Photon_fBits);
   fChain->SetBranchAddress("Photon.PT", Photon_PT, &b_Photon_PT);
   fChain->SetBranchAddress("Photon.Eta", Photon_Eta, &b_Photon_Eta);
   fChain->SetBranchAddress("Photon.Phi", Photon_Phi, &b_Photon_Phi);
   fChain->SetBranchAddress("Photon.E", Photon_E, &b_Photon_E);
   fChain->SetBranchAddress("Photon.T", Photon_T, &b_Photon_T);
   fChain->SetBranchAddress("Photon.EhadOverEem", Photon_EhadOverEem, &b_Photon_EhadOverEem);
   fChain->SetBranchAddress("Photon.Particles", Photon_Particles, &b_Photon_Particles);
   fChain->SetBranchAddress("Photon.IsolationVar", Photon_IsolationVar, &b_Photon_IsolationVar);
   fChain->SetBranchAddress("Photon.IsolationVarRhoCorr", Photon_IsolationVarRhoCorr, &b_Photon_IsolationVarRhoCorr);
   fChain->SetBranchAddress("Photon.SumPtCharged", Photon_SumPtCharged, &b_Photon_SumPtCharged);
   fChain->SetBranchAddress("Photon.SumPtNeutral", Photon_SumPtNeutral, &b_Photon_SumPtNeutral);
   fChain->SetBranchAddress("Photon.SumPtChargedPU", Photon_SumPtChargedPU, &b_Photon_SumPtChargedPU);
   fChain->SetBranchAddress("Photon.SumPt", Photon_SumPt, &b_Photon_SumPt);
   fChain->SetBranchAddress("Photon.Status", Photon_Status, &b_Photon_Status);
   fChain->SetBranchAddress("Photon_size", &Photon_size, &b_Photon_size);
   fChain->SetBranchAddress("Muon", &Muon_, &b_Muon_);
   fChain->SetBranchAddress("Muon.fUniqueID", Muon_fUniqueID, &b_Muon_fUniqueID);
   fChain->SetBranchAddress("Muon.fBits", Muon_fBits, &b_Muon_fBits);
   fChain->SetBranchAddress("Muon.PT", Muon_PT, &b_Muon_PT);
   fChain->SetBranchAddress("Muon.Eta", Muon_Eta, &b_Muon_Eta);
   fChain->SetBranchAddress("Muon.Phi", Muon_Phi, &b_Muon_Phi);
   fChain->SetBranchAddress("Muon.T", Muon_T, &b_Muon_T);
   fChain->SetBranchAddress("Muon.Charge", Muon_Charge, &b_Muon_Charge);
   fChain->SetBranchAddress("Muon.Particle", Muon_Particle, &b_Muon_Particle);
   fChain->SetBranchAddress("Muon.IsolationVar", Muon_IsolationVar, &b_Muon_IsolationVar);
   fChain->SetBranchAddress("Muon.IsolationVarRhoCorr", Muon_IsolationVarRhoCorr, &b_Muon_IsolationVarRhoCorr);
   fChain->SetBranchAddress("Muon.SumPtCharged", Muon_SumPtCharged, &b_Muon_SumPtCharged);
   fChain->SetBranchAddress("Muon.SumPtNeutral", Muon_SumPtNeutral, &b_Muon_SumPtNeutral);
   fChain->SetBranchAddress("Muon.SumPtChargedPU", Muon_SumPtChargedPU, &b_Muon_SumPtChargedPU);
   fChain->SetBranchAddress("Muon.SumPt", Muon_SumPt, &b_Muon_SumPt);
   fChain->SetBranchAddress("Muon.D0", Muon_D0, &b_Muon_D0);
   fChain->SetBranchAddress("Muon.DZ", Muon_DZ, &b_Muon_DZ);
   fChain->SetBranchAddress("Muon.ErrorD0", Muon_ErrorD0, &b_Muon_ErrorD0);
   fChain->SetBranchAddress("Muon.ErrorDZ", Muon_ErrorDZ, &b_Muon_ErrorDZ);
   fChain->SetBranchAddress("Muon_size", &Muon_size, &b_Muon_size);
   fChain->SetBranchAddress("MissingET", &MissingET_, &b_MissingET_);
   fChain->SetBranchAddress("MissingET.fUniqueID", MissingET_fUniqueID, &b_MissingET_fUniqueID);
   fChain->SetBranchAddress("MissingET.fBits", MissingET_fBits, &b_MissingET_fBits);
   fChain->SetBranchAddress("MissingET.MET", MissingET_MET, &b_MissingET_MET);
   fChain->SetBranchAddress("MissingET.Eta", MissingET_Eta, &b_MissingET_Eta);
   fChain->SetBranchAddress("MissingET.Phi", MissingET_Phi, &b_MissingET_Phi);
   fChain->SetBranchAddress("MissingET_size", &MissingET_size, &b_MissingET_size);
   fChain->SetBranchAddress("ScalarHT", &ScalarHT_, &b_ScalarHT_);
   fChain->SetBranchAddress("ScalarHT.fUniqueID", ScalarHT_fUniqueID, &b_ScalarHT_fUniqueID);
   fChain->SetBranchAddress("ScalarHT.fBits", ScalarHT_fBits, &b_ScalarHT_fBits);
   fChain->SetBranchAddress("ScalarHT.HT", ScalarHT_HT, &b_ScalarHT_HT);
   fChain->SetBranchAddress("ScalarHT_size", &ScalarHT_size, &b_ScalarHT_size);
   Notify();
}

Bool_t Analysis::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void Analysis::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t Analysis::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef Analysis_cxx
