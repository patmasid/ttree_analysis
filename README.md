## How to install the repository

```
git clone ssh://git@gitlab.cern.ch:7999/patmasid/ttree_analysis.git
```

## Running the code

```
cd ttree_analysis
make
```

This compiles the code and creates an executable called ```execute```.

Currently the input root file name is hard-coded inside the script. ```tag_1_delphes_events.root```. You have to have this root file in the your working directory along with all the ```.cxx``` and ```.h``` files.

To run the script you do:

```
./execute
```